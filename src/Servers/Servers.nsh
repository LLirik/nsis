!ifndef SERVERS_NSH
!define SERVERS_NSH

;==========================================================================
; ����������� ��������������� �������

  !include "Plugins\include\ValidateIPAdress.nsh"      ;�������� ������������ ����� IP-������
  

;==========================================================================
;���� �������

;--------------------------------------------------------------------------
;�������, ���������� ����� ����� ������� ���������
/*
  Function .onInit
           ;��������� ������ �� �������� ����������� �� �������, ���� ����

           ReadRegStr $IP_Srv1_State HKLM "Software\Cometa\��� ���� ���" "IP_srv1"
           ${If} $IP_Srv1_State == ""
                 StrCpy $IP_Srv1_State "172.16.0.130"
           ${EndIf}

           ReadRegStr $IP_Srv2_State HKLM "Software\Cometa\��� ���� ���" "IP_srv2"
           ${If} $IP_Srv2_State == ""
                 StrCpy $IP_Srv2_State "172.16.0.130"
           ${EndIf}

           ReadRegStr $Srv1_Usr_State HKLM "Software\Cometa\��� ���� ���" "Srv1_usr"
           ${If} $Srv1_Usr_State == ""
                 StrCpy $Srv1_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv1_Pswd_State HKLM "Software\Cometa\��� ���� ���" "Srv1_pswd"
           ${If} $Srv1_Pswd_State == ""
                 StrCpy $Srv1_Pswd_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv2_Usr_State HKLM "Software\Cometa\��� ���� ���" "Srv2_usr"
           ${If} $Srv2_Usr_State == ""
                 StrCpy $Srv2_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv2_Pswd_State HKLM "Software\Cometa\��� ���� ���" "Srv2_pswd"
           ${If} $Srv2_Pswd_State == ""
                 StrCpy $Srv2_Pswd_State "mkpa"
           ${EndIf}

  FunctionEnd
  
  */
/*
  TODO ����������� ���������� ��� ������ ����� �������.
       ��� �����, ������ ����������� �������� � ���� ��� � "���������"
       �� TextBox
  end TODO
*/

/**************************************************************************************************/
;--------------------------------------------------------------------------
;������� ����������� ��������� 2-�� �������

  Function OnOffSrv2Box
	Pop $CheckBoxOff2
	${NSD_GetState} $CheckBoxOff2 $0
	${If} $0 == 0
		EnableWindow $IP_Srv2 1
		EnableWindow $Srv2_Usr 1
		EnableWindow $Srv2_Pswd 1
		EnableWindow $CheckBoxSrv2 1
	        call Srv2IPAddrTextChange
	${Else}
		EnableWindow $IP_Srv2 0
		EnableWindow $Srv2_Usr 0
		EnableWindow $Srv2_Pswd 0
		EnableWindow $CheckBoxSrv2 0
		call Srv2IPAddrTextChange
	${EndIf}

  FunctionEnd

;--------------------------------------------------------------------------
;������� ����������� ��������� 1-�� �������

  Function OnOffSrv1Box
	Pop $CheckBoxOff1
	${NSD_GetState} $CheckBoxOff1 $0
	${If} $0 == 0
		EnableWindow $IP_Srv1 1
		EnableWindow $Srv1_Usr 1
		EnableWindow $Srv1_Pswd 1
		EnableWindow $CheckBoxSrv1 1
		call Srv1IPAddrTextChange
	${Else}
		EnableWindow $IP_Srv1 0
		EnableWindow $Srv1_Usr 0
		EnableWindow $Srv1_Pswd 0
		EnableWindow $CheckBoxSrv1 0
		call Srv2IPAddrTextChange
	${EndIf}

  FunctionEnd

/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/
;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� 2-�� �������

  Function EnDisableSrv2Box
	Pop $CheckBoxSrv2
	${NSD_GetState} $CheckBoxSrv2 $0
	${If} $0 == 0
		EnableWindow $IP_Srv2 1
		EnableWindow $Srv2_Usr 1
		EnableWindow $Srv2_Pswd 1
		
	        call Srv2IPAddrTextChange
	${Else}
		EnableWindow $IP_Srv2 0
		EnableWindow $Srv2_Usr 0
		EnableWindow $Srv2_Pswd 0
		
		call Srv2IPAddrTextChange
	${EndIf}


  FunctionEnd

;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� �������

  Function EnDisableSrv1Box
	Pop $CheckBoxSrv1
	${NSD_GetState} $CheckBoxSrv1 $0
	${If} $0 == 0
		EnableWindow $IP_Srv1 1
		EnableWindow $Srv1_Usr 1
		EnableWindow $Srv1_Pswd 1
		call Srv1IPAddrTextChange
	${Else}
		EnableWindow $IP_Srv1 0
		EnableWindow $Srv1_Usr 0
		EnableWindow $Srv1_Pswd 0
		call Srv2IPAddrTextChange
	${EndIf}

  FunctionEnd
  /**************************************************************************************************/
;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������ �������
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function Srv2IPAddrTextChange
  
        ClearErrors
        ;Pop $1 # $1 == $IP_mon
        Pop $IP_Srv2 # $1 == $IP_mon
        
	${NSD_GetText} $IP_Srv2 $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $Srv2_Err "1"                 #
        ${Else}
              StrCpy $Srv2_Err "0"
        ${EndIf}                                  #

        Call EnDisableNextButton
        
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������ �������
;����� ���������� ������ ��� ����� ����� � ���� ���������

 Function Srv1IPAddrTextChange

        ClearErrors
        ;Pop $1 # $1 == $IP_srv
        Pop $IP_Srv1 # $1 == $IP_srv

	${NSD_GetText} $IP_Srv1 $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $Srv1_Err "1"                 #
        ${Else}
              StrCpy $Srv1_Err "0"
        ${EndIf}                                  #

        Call EnDisableNextButton

  FunctionEnd
;--------------------------------------------------------------------------
;�������, ������������� ������ "�����" ��� ��������� �������� IP ������

  Function EnDisableNextButton

        GetDlgItem $NextButton $HWNDPARENT 1

       	Pop $CheckBoxSrv1
	${NSD_GetState} $CheckBoxSrv1 $1

	Pop $CheckBoxSrv2
	${NSD_GetState} $CheckBoxSrv2 $2

        IntOp $1 $1 !
        IntOp $2 $2 !

        IntOp $Case1 $Srv1_Err && $1
        IntOp $Case2 $Srv2_Err && $2

        IntOp $Double_Err $Case1 || $Case2

        ${If} $Double_Err == 1
              EnableWindow $NextButton 0
        ${Else}
              EnableWindow $NextButton 1
              ;MessageBox MB_OK "�� ������"
        ${EndIf}
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ��������� ��������

  Function ServersPageCreate
  
        SectionGetFlags ${Servers} $R0
        Pop $R0
        ${If} $R0 == 1
              goto show
        ${EndIf}
        abort
        show:
        
	nsDialogs::Create 1018        ;������� ��������, � "���������" � �����
	Pop $Dialog                   ;����������� ��� � ����������

	${If} $Dialog == error
              MessageBox MB_OK "�������� �������� ��� ���������. ���������� � ������������. $\n $Dialog"
	      Abort
	${EndIf}

	${NSD_CreateLabel} 15% 0% 100% 18% "����� ������������ ��������� �������� ���������. $\n\
                                         �������� �������� ���������: �������� ��� ���������.$\n\
                                         ��� ������������� ������� IP-����� ��������(��) �����(�)."
	Pop $Label
;=================================================================================================================
	${NSD_CreateCheckBox} 2% 27% 42% 13% "�������� ��� ��������� ���������� ������� �������."
        Pop $CheckBoxSrv1
	    ${NSD_OnClick} $CheckBoxSrv1 EnDisableSrv1Box ;TODO ������� ����� ����� �������� EnDisableBox
	    
	${NSD_CreateCheckBox} 53%% 27% 42% 13% "��������, ��� ��������� ���������� ������� �������."
        Pop $CheckBoxSrv2
	    ${NSD_OnClick} $CheckBoxSrv2 EnDisableSrv2Box ;TODO ������� ����� ����� �������� EnDisableBox
/*******************************************     GROUP BOXES    **************************************************/
        ${NSD_CreateGroupBox} 0% 19% 49% 70% "�������� ��������� ������� �������"
        Pop $GBox
        
        ${NSD_CreateGroupBox} 50% 19% 50% 70% "�������� ��������� ������� �������"
        Pop $GBox
/*******************************************     IP �������1    ***************************************************/
	${NSD_CreateLabel} 2% 45% 13% 9% "IP-�����:"
	Pop $Label

	${NSD_CreateText} 15% 44% 32% 9% $IP_Srv1_State
	Pop $IP_Srv1
/*******************************************     IP �������2   ****************************************************/
	${NSD_CreateLabel} 53% 45% 13% 9% "IP-�����:"
	Pop $Label

	${NSD_CreateText} 67% 44% 31% 9% $IP_Srv2_State
        Pop $IP_Srv2
/*******************************************  ����� �� �������1  ***************************************************/
	${NSD_CreateLabel} 2% 57% 13% 9% "�����:"
	Pop $Label

        ${NSD_CreateText} 15% 55% 32% 9% $Srv1_Usr_State
        Pop $Srv1_usr
/*******************************************  ����� �� �������2  **************************************************/
	${NSD_CreateLabel} 53% 57% 13% 9% "�����:"
	Pop $Label

        ${NSD_CreateText} 67% 55% 31% 9% $Srv2_Usr_State
        Pop $Srv2_Usr
/*******************************************  ������ �� �������1  **************************************************/
        ${NSD_CreateLabel} 2% 67% 13% 9% "������:"
	Pop $Label
        
        ${NSD_CreateText} 15% 66% 32% 9% $Srv1_Pswd_State
        Pop $Srv1_Pswd
/*******************************************  ������ �� �������2  **************************************************/
        ${NSD_CreateLabel} 53% 67% 13% 9% "������:"
	Pop $Label

        ${NSD_CreateText} 67% 66% 31% 9% $Srv2_Pswd_State
        Pop $Srv2_Pswd
/******************************************** ON/OFF CheckBoxes ****************************************************/
	${NSD_CreateCheckBox} 2% 74% 42% 13% "�� ������������� 1-� ������."
        Pop $CheckBoxOff1
	    ${NSD_OnClick} $CheckBoxOff1 OnOffSrv1Box ;TODO ������� ����� ����� �������� EnDisableBox

	${NSD_CreateCheckBox} 53%% 74% 42% 13% "�� ������������� 2-� ������"
        Pop $CheckBoxOff2
	    ${NSD_OnClick} $CheckBoxOff2 OnOffSrv2Box ;TODO ������� ����� ����� �������� EnDisableBox

;=====================================================================================================================

        ; �� ��������� IP-������ ���������
        EnableWindow $IP_Srv2 1
        EnableWindow $IP_Srv1 1
         
        EnableWindow $Srv1_Usr 1
        EnableWindow $Srv2_Usr 1
        
        EnableWindow $Srv1_Pswd 1
        EnableWindow $Srv2_Pswd 1

        ${NSD_OnChange} $IP_Srv2 Srv2IPAddrTextChange
        ${NSD_OnChange} $IP_Srv1 Srv1IPAddrTextChange
        
        ${If} $CheckBoxOff2_State == ${BST_CHECKED}
       		${NSD_Check} $CheckBoxOff2
                EnableWindow $IP_Srv2 0
                EnableWindow $Srv2_Usr 0
                EnableWindow $Srv2_Pswd 0
                EnableWindow $CheckBoxSrv2 0
	    ${EndIf}

        ${If} $CheckBoxOff1_State == ${BST_CHECKED}
       		${NSD_Check} $CheckBoxOff1
                EnableWindow $IP_Srv1 0
                EnableWindow $Srv1_Usr 0
                EnableWindow $Srv1_Pswd 0
                EnableWindow $CheckBoxSrv1 0
	    ${EndIf}
	
        ${If} $CheckBoxSrv2_State == ${BST_CHECKED}
        ${AndIf} $CheckBoxOff2_State != ${BST_CHECKED}
		${NSD_Check} $CheckBoxSrv2
                EnableWindow $IP_Srv2 0
                EnableWindow $Srv2_Usr 0
                EnableWindow $Srv2_Pswd 0
	     ${EndIf}

	${If} $CheckBoxSrv1_State == ${BST_CHECKED}
        ${AndIf} $CheckBoxOff1_State != ${BST_CHECKED}
		${NSD_Check} $CheckBoxSrv1
		EnableWindow $IP_Srv1 0
		EnableWindow $Srv1_Usr 0
		EnableWindow $Srv1_Pswd 0
	${EndIf}

        call Srv1IPAddrTextChange
        call Srv2IPAddrTextChange
        
	nsDialogs::Show

  FunctionEnd
;--------------------------------------------------------------------------
;�������, �������������� ��� ����� �� ��������
  Function ServersPageLeave
  
           ${NSD_GetState} $CheckBoxOff2 $CheckBoxOff2_State
           ${If} $CheckBoxOff2_State != ${BST_CHECKED}
                  ${NSD_GetState} $CheckBoxSrv2 $CheckBoxSrv2_State
                  ${If} $CheckBoxSrv2_State != ${BST_CHECKED}

                  ${Else}
                         StrCpy $IP_Srv2_State "192.168.1.201"
                         ${NSD_SetText} $IP_Srv2 $IP_Srv2_State
	          ${EndIf}
           ${EndIf}
           
           ${NSD_GetState} $CheckBoxOff1 $CheckBoxOff1_State
           ${If} $CheckBoxOff1_State != ${BST_CHECKED}
                  ${NSD_GetState} $CheckBoxSrv1 $CheckBoxSrv1_State
	          ${If} $CheckBoxSrv1_State != ${BST_CHECKED}

	          ${Else}
                        StrCpy $IP_Srv1_State "192.168.1.101"
                        ${NSD_SetText} $IP_Srv1 $IP_Srv1_State
	          ${EndIf}
           ${EndIf}
	
        ${NSD_GetText} $IP_Srv1 $IP_Srv1_State
        ${NSD_GetText} $IP_Srv2 $IP_Srv2_State
        
        ${NSD_GetText} $Srv1_Usr $Srv1_Usr_State
        ${NSD_GetText} $Srv1_Pswd $Srv1_Pswd_State

        ${NSD_GetText} $Srv2_Usr $Srv2_Usr_State
        ${NSD_GetText} $Srv2_Pswd $Srv2_Pswd_State
        
        ${NSD_GetState} $CheckBoxSrv1 $CheckBoxSrv1_State
        ${NSD_GetState} $CheckBoxSrv2 $CheckBoxSrv2_State
        
        ${NSD_GetState} $CheckBoxOff1 $CheckBoxOff1_State
        ${NSD_GetState} $CheckBoxOff2 $CheckBoxOff2_State
       ; MessageBox MB_OK "V(^__^)V $CheckBoxOff1_State $CheckBoxOff2_State"

        ;MessageBox MB_OK "(V)(^,,,^)(V)$\n $\n   \(0o0)/ $\n $Srv_usr_State $\n $Srv_pswd_State $\n $Mon_usr_State $\n $Mon_pswd_State"

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv1" $IP_Srv1_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv2" $IP_Srv2_State

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Srv1_usr" $Srv1_Usr_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Srv1_pswd" $Srv1_Pswd_State
        
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Srv2_usr" $Srv2_Usr_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Srv2_pswd" $Srv2_Pswd_State
        
        StrCpy $Double_Err "0"
        DetailPrint "����� �������� �������: $IP_Srv1_State"
        DetailPrint "����� ������� �������: $IP_Srv2_State"
        ;����-�������� 
        ;MessageBox MB_OK "V(^__^)V $\n����� �������: $IP_srv_State$\n$\n����� ��������: $IP_mon_State$\n$\n"
  FunctionEnd
;==========================================================================
;MessageBox MB_OK "V(^__^)V" - ����-��������
;MessageBox MB_OK "(V)(^,,,^)(V)" - �����-������-��������
;MessageBox MB_OK "\(0o0)/ - ���� ��������"

;MessageBox MB_OK  (\.../) (\.../) (\.../) (\.../) $\n ( *.*) ('.'= ) ('.'= ) ('.'= ) $\n (")_(") (")_(") (")_(") (")_(") $\n ��������� ��������-����������"
!endif