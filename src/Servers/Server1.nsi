	!include LogicLib.nsh
	!include FileFunc.nsh

        /************ WTF ************/
	!insertmacro Locate
    
	!define PROJECT_ROOT "..\.."

        !ifndef TARGET
	        !define TARGET "target_template"
	!endif

        !define TARGET_ROOT "${PROJECT_ROOT}\${TARGET}\������ 1"
	!define SERVICE_NAME "QCOMServer1"
	!define PROJECT_NAME "��� ���� ���"
	!define INSTDIR "������ 1"
	!define COMAPP_NAME "comapp1"
	!define COMAPP_GUID "{B7DA3DE8-83BB-4BBE-9AB7-99A05819E201}"

        InstallDir "$PROGRAMFILES64\Cometa\${PROJECT_NAME}\${INSTDIR}"
	
	!addplugindir "Plugins\bin"
	!addplugindir "${PROJECT_ROOT}\plugins\bin"

	;--------------------------------
	Name "${PROJECT_NAME}"
	OutFile "${PROJECT_ROOT}\server1.exe"
	;--------------------------------
	SetDatablockOptimize on /************ WTF ************/
	XPStyle on
	AutoCloseWindow false
	ShowInstDetails SHOW
	;SetCompressor lzma
	
	BrandingText "��� $\"���������� ������$\" ����-44$\n2011-2015"
	LoadLanguageFile "${NSISDIR}\Contrib\Language files\Russian.nlf"
	;--------------------------------
	RequestExecutionLevel admin
	;--------------------------------
	; Pages

	Page components
	Page directory
	Page instfiles

	UninstPage uninstConfirm
	UninstPage instfiles    

	;--------------------------------
	;!include EnvVarUpdate.nsh
	;--------------------------------
	Var /GLOBAL switch_overwrite
	!include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"	
	;--------------------------------
	;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	;  ��������� ������ ������ ����
	!include "..\mainServerSrc.nsh"	
	;vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	;--------------------------------
	; 
	Section ""
		Push 0
		Call FindAndDestroyService
		RMDir /r "$INSTDIR_backup"
		StrCpy $switch_overwrite 1 
		!insertmacro MoveFolder "$INSTDIR" "$INSTDIR_backup" "*.*"
	SectionEnd
    
	SectionGroup  "��������"
		Section /o "OmniBusBox" OmniSec
			;SectionSetFlags SF_SELECTED 0
			;SectionSetFlags SF_SELECTED 1
			SetOutPath "$TEMP"
			FILE /r "${PROJECT_ROOT}\Vendor\OMNIBUSBOX\WINDOWS32"
			ExecWait "$TEMP\WINDOWS32\setup.exe /passive"

			;RMDir /r "$TEMP\WINDOWS32"
			SetOutPath "$PROGRAMFILES\OMNIBUSBOX\API\"
			FILE /a /r "$%OMNIBUS_HOME%\*.*"
			WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "OMNIBUS_HOME" '$PROGRAMFILES\OMNIBUSBOX\API'
		SectionEnd
		
		Section /o "�������� ���������" Stubs
			;IfSilent 0 +2
			;	SectionSetFlags SF_SELECTED 0
			;SectionSetFlags SF_SELECTED 1
			SetOutPath "$INSTDIR"
			FILE /r "${PROJECT_ROOT}\${TARGET}\��������\*.*"
		SectionEnd
	SectionGroupEnd
	;--------------------------------
	Section "������ ������"
		;SectionSetFlags SF_SELECTED
		
                Push 1
		Call FindAndDestroyService
        
		SetOutPath "$TEMP"
		FILE /r "${PROJECT_ROOT}\Vendor\vcredist_x86.exe"
		ExecWait "$TEMP\vcredist_x86.exe /q"
		DELETE "$TEMP\vcredist_x86.exe"

		SetOutPath "$INSTDIR"
		
		File /r "${TARGET_ROOT}\*.*"

		File "${PROJECT_ROOT}\img\server.ico"
		File "${PROJECT_ROOT}\vendor\restart.bat"

		FILE "${PROJECT_ROOT}\Vendor\regtlibv12.exe"
        
                ;������ ������� �� ������!
		Call SetServerPermissions		
		Call ServerShortCut          ;��� ������� ����� �������� ������ ����� �������� mkpa|mkpa
		
		WriteUninstaller "$INSTDIR\uninst_server.exe"
		
		ClearErrors
		SimpleSC::StartService ${SERVICE_NAME} "" 30 ;������ ������� ��� ���������� � ��������� � 30 ������
		Pop $0
		${If} $0 != 0
			IfSilent +2
			MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${SERVICE_NAME}.$\n ������=$0 �������� �� ������� ���������$\n��������� ������ �������."
		${EndIf}

        #TODO � ������� � ������ ���� ���� � ��������� ���������� ���������?
        ;SetOutPath "C:\VXIPNP\WINNT\bin"

        ;DetailPrint "*******���������� ��������� ������� ���������*******"
        ;FILE /r   "�� ����\��������\�������� ��������\*.dll"
	SectionEnd
	
	Section ""
		/*� ������ �� ����� ����� ��� �����. �� ��� ����� ����!*/
        	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Policies\System" "LocalAccountTokenFilterPolicy" 0x1
        	;������ ��������, �� ��������� �� ����� �����
    	        WriteRegStr HKLM "Software\Cometa\${PROJECT_NAME}" "ag_port" "5025"
    	        WriteRegStr HKLM "Software\Cometa\${PROJECT_NAME}" "ag_addr" "192.168.1.105"
    	      /*
                ;�� ������ ������ ���������
    	        WriteRegStr HKLM "Software\Cometa\${PROJECT_NAME}" "osc_addr" "192.168.1.41"
    	        WriteRegStr HKLM "Software\Cometa\${PROJECT_NAME}" "an_addr" "192.168.1.218"
    	        WriteRegStr HKLM "Software\Cometa\${PROJECT_NAME}" "ps_port" "5025"
    	        WriteRegStr HKLM "Software\Cometa\${PROJECT_NAME}" "ps_addr" "192.168.1.205"
    	        */
	SectionEnd

	Function .onGUIEnd
		SectionGetFlags ${OmniSec} $0
		IntOp $0 $0 & 1
		StrCmp $0 0 0 +2
		Abort
		MessageBox MB_YESNO|MB_ICONEXCLAMATION "������ ��������� �������������� � OmniBusBox? $\n������� '��' ���� ������� �������������� ������ ������."  IDNO +2
		ExecWait '"$PROGRAMFILES\BTITST32\BTITST32.EXE"'
        FunctionEnd
; Uninstaller

        Var ProgmanHwnd
        Var ShellHwnd
        Var DesktopHwnd

        Function un.RefreshDesktop
            ; 256=WM_KEYDOWN
            ; 257=WM_KEYUP
            ; 116=VK_F5
            FindWindow $ProgmanHwnd "Progman" "Program Manager"
            FindWindow $ShellHwnd "SHELLDLL_DefView" "" $ProgmanHwnd 0
            FindWindow $DesktopHwnd "SysListView32" "" $ShellHwnd 0
            System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 256, i 116, i 0)"
            System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 257, i 116, i 0x80000000)"
        FunctionEnd

	UninstallText "�������� ������� �������"
	UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\orange-uninstall.ico"

	Section "Uninstall"
	
		DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PROJECT_NAME}"
		
		call un.FindAndDestroyService
		
		Delete "$INSTDIR\*.*"
		Delete "$INSTDIR\uninst_server.exe"
		Delete /REBOOTOK "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}\*.*"
		RMDir /r "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}"
		
 		Delete "$DESKTOP\${INSTDIR} (������ �������).lnk"
		Delete "$DESKTOP\${INSTDIR} ���������.lnk"

		call un.RefreshDesktop
		RMDir /r "$INSTDIR"
		
		IfFileExists "$INSTDIR\������ 1" 0 NoErrorMsg
			MessageBox MB_OK "��������: ����� $INSTDIR\������ ������ �� ������� ���������!!!" IDOK 0

		NoErrorMsg:
	SectionEnd
