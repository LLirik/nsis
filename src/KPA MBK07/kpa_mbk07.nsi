!include LogicLib.nsh
!include FileFunc.nsh
!insertmacro Locate

!define PROJECT_ROOT "..\.."
!ifndef TARGET
!define TARGET "target_template"
!endif
!define TARGET_ROOT "${PROJECT_ROOT}\${TARGET}\���07"
	!define SERVICE_NAME "QCOMMbk07"
	!define INSTDIR "���07"
	!define COMAPP_NAME "mbk07"
	!define COMAPP_GUID "{b50292b7-c942-4FDC-Ab73-817a971a2ea0}"
	!define SECTIONCOUNT 5
	InstallDir "$PROGRAMFILES64\Cometa\��� ���� ���\${INSTDIR}"

	!addplugindir "Plugins\bin"
	!addplugindir "${PROJECT_ROOT}\plugins\bin"

;-------------------------------------------------------------------------------
;Name and file_name
        Name "��� ���� ���"
	OutFile "${PROJECT_ROOT}\MBK07.exe"

;-------------------------------------------------------------------------------
;Visual information
	SetDatablockOptimize on
	XPStyle on
	AutoCloseWindow false
	ShowInstDetails show
        SetCompressor lzma

	BrandingText "��� $\"���������� ������$\" ����-44$\n2011-2015"
	LoadLanguageFile "${NSISDIR}\Contrib\Language files\Russian.nlf"

;-------------------------------------------------------------------------------
;������� ����� �������������� �� ���������

	RequestExecutionLevel admin

#-------------------------------------------------------------------------------
#
#       Pages
#
#-------------------------------------------------------------------------------

	Page components
	Page directory
	Page instfiles

	UninstPage uninstConfirm
	UninstPage instfiles

#-------------------------------------------------------------------------------

	Var /GLOBAL switch_overwrite
	!include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"
	
#-------------------------------------------------------------------------------
#
#       Functions
#
#-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;����������� � ��������� (���� ����������) ������

!macro FindAndDestroyServiceMACRO un
		Function "${un}FindAndDestroyService"
			ClearErrors
			DetailPrint "����� ��������� ${SERVICE_NAME}"
			SimpleSC::GetServiceStatus ${SERVICE_NAME}
			Pop $0 ; ���������� ��� ������ ���� ������� �� ����������: (!=0) � ��������� ������ ����� �����: (0)
			Pop $1 ; ���������� ������ ������� (�� ����:)
			DetailPrint "��� ���������:$\n ��� ������: $0 $\n ������ ������ ${SERVICE_NAME}: $1"
			# MessageBox MB_OK|MB_ICONEXCLAMATION "��� ���������:$\n ��� ������: $0 $\n ������ ������: $1"
			/*  1 - STOPPED
				2 - START_PENDING
				3 - STOP_PENDING
				4 - RUNNING
				5 - CONTINUE_PENDING
				6 - PAUSE_PENDING
				7 - PAUSED
			*/
			${If} $0 == 0      ; ���� ������ ����, ����� ��������� �� ������
				${If} $1 == 4
					DetailPrint "${SERVICE_NAME} ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"
					#MessageBox MB_OK|MB_ICONEXCLAMATION "${SERVICE_NAME} ?������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"

					SimpleSC::StopService ${SERVICE_NAME} 1 30 ; �����, ����� ����������� ���� � ������� 30 ������.
					Pop $R0
					########################################################
                                        ExecWait "taskkill /F /IM '${COMAPP_NAME}.exe'" #���� ����� ��� �� ������ - ����� � ������!!!
					StrCmp $R0 0 dead_meat why_wont_you_die ;��������� ��������� ��� �������� �������� � ������ �� �� �������
                                        why_wont_you_die:
						MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${SERVICE_NAME}. ��������� ������� �������,$\n����� ���� �������  ��."
					dead_meat:
						DetailPrint  "������ ${SERVICE_NAME} ������� ����������� � ����� �������!"
				${Else}
					DetailPrint  "������ ${SERVICE_NAME} �� ���� �������� � ����� �������!"
				${EndIf}
			${EndIf}
			Pop $R1

			${If} $R1 == 1
				ExecWait "sc delete ${SERVICE_NAME}"
			${EndIf}
			ExecWait "taskkill /F /IM '${COMAPP_NAME}.exe'" #���� ����� ���-�� ������, ��� �� ������ - ��� ����� ����� ������� �� �������!
		
		FunctionEnd
	!macroend

	!insertmacro FindAndDestroyServiceMACRO ""
	!insertmacro FindAndDestroyServiceMACRO "un."
	
;-------------------------------------------------------------------------------
;�������� �������
        
	Function "ServerShortCut"
        ;SetShellVarContext all
		CreateDirectory "$SMPROGRAMS\��� ���� ���\${INSTDIR}"
		
		CreateShortCut  "$SMPROGRAMS\��� ���� ���\${INSTDIR}\������� ������ ${INSTDIR}.lnk" "$INSTDIR\uninst_MBK07.exe"
		CreateShortCut  "$SMPROGRAMS\��� ���� ���\${INSTDIR}\������ ${INSTDIR}(������ �������).lnk" "$INSTDIR\${COMAPP_NAME}.exe" -i "$INSTDIR\server1.ico"
		CreateShortCut  "$SMPROGRAMS\��� ���� ���\${INSTDIR}\������ ${INSTDIR} ���������.lnk" "$INSTDIR\${COMAPP_NAME}.exe" "-e" "$INSTDIR\server.ico"
		
                CreateShortCut  "$DESKTOP\������ ${INSTDIR} (������ �������).lnk" "$INSTDIR\${COMAPP_NAME}.exe" -i "$INSTDIR\server1.ico"
		CreateShortCut  "$DESKTOP\������ ${INSTDIR} ���������.lnk" "$INSTDIR\${COMAPP_NAME}.exe" "-e" "$INSTDIR\server.ico"
	
	;SetShellVarContext current
	FunctionEnd

;-------------------------------------------------------------------------------
;��������� ���� ����������� ���������� � ���� ��� ������

	Function "SetServerPermissions"

		ExecWait "net user /expires:never /add mkpa mkpa"  ; ������ ������������ mkpa � ������� mkpa. ���� ��������� ������ �����������
		ExecWait "WMIC USERACCOUNT WHERE Name='mkpa' SET PasswordExpires=FALSE"
                ExecWait "net localgroup �������������� mkpa /add" ; ��������� ������������ � ������ ���������������
                
		IfErrors 0 +2
		         ExecWait "net localgroup Administrators mkpa /add"
		ClearErrors

		SetOutPath "$TEMP"
		FILE /r "${PROJECT_ROOT}\Vendor\dcomperm.exe"
		FILE /r "${PROJECT_ROOT}\Vendor\ntrights.exe"

                ExecWait "$INSTDIR\${COMAPP_NAME}.exe /regserver"
                
                ExecWait 'sc delete MBK07'
		ExecWait 'sc create ${SERVICE_NAME} binPath= "$INSTDIR\${COMAPP_NAME}.exe"' ; ������ ������
		ExecWait "sc config ${SERVICE_NAME} obj= .\mkpa password= mkpa" ; ������ �� ����� ������������ ����

                ExecWait "dcomperm.exe -al ${COMAPP_GUID} set mkpa permit level:ll,rl,la,ra" ; ����� �� ������� ������ � ��.
                ExecWait `dcomperm.exe -al ${COMAPP_GUID} set "���" permit level:ll,rl,la,ra`
                ExecWait `dcomperm.exe -al ${COMAPP_GUID} set "REMOTE INTERACTIVE LOGON" permit level:ll,rl,la,ra`
                ExecWait `dcomperm.exe -al ${COMAPP_GUID} set "��������� ����" permit level:ll,rl,la,ra`

                ExecWait "$TEMP\ntrights.exe +r SeServiceLogonRight -u mkpa" ; ���������� �� ������ � �������� ������
                FILE "${PROJECT_ROOT}\Vendor\Lsa_new.reg"
                ExecWait "Regedit /s Lsa_new.reg" ; ��������� ������ � ������ � ������
                Delete $INSTDIR\Lsa_new.reg

               

                ExecWait "$INSTDIR\regtlibv12.exe $INSTDIR\${COMAPP_NAME}.tlb"

                ;ExecWait "sc config QCOMMonitor start= auto"
		;ExecWait "sc config MBK07 start= auto"
	
	FunctionEnd

#-------------------------------------------------------------------------------
#
#        Sections
#
#-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
	Section "" save_old
                RmDir "$INSTDIR\backup_���07"
                Call FindAndDestroyService

                StrCpy $switch_overwrite 1 #0- ������������ ������ ������ ����� ������. 1- �� ������������
                !insertmacro MoveFolder "$INSTDIR\���07\" "$INSTDIR\backup_���07" "*.*"
                IfFileExists "$INSTDIR\backup_���07" 0 +2
                    RmDir "$INSTDIR\backup_���07"
	SectionEnd

        Section "������ �� ���07" MBK07Sect
                Push 1 #������� ������
		Call FindAndDestroyService
		SetOutPath "$TEMP"
		
		FILE "${PROJECT_ROOT}\Vendor\vcredist_x86.exe"
		ExecWait "$TEMP\vcredist_x86.exe /q"
		DELETE "$TEMP\vcredist_x86.exe"

 		SetOutPath "$INSTDIR"
                File "${PROJECT_ROOT}\img\server.ico"
                File "${PROJECT_ROOT}\img\server1.ico"

                FILE "${TARGET_ROOT}\${COMAPP_NAME}.exe"
		File "${TARGET_ROOT}\${COMAPP_NAME}.tlb"

		FILE "${TARGET_ROOT}\msvcr100d.dll"
                FILE "${TARGET_ROOT}\msvcp100d.dll"

                FILE "${TARGET_ROOT}\QtCored4.dll"
                FILE "${TARGET_ROOT}\QtGuid4.dll"
                FILE "${TARGET_ROOT}\QtNetworkd4.dll"

                FILE "${PROJECT_ROOT}\Vendor\regtlibv12.exe"


                Call SetServerPermissions
                Call ServerShortCut
                
                ClearErrors
  	        SimpleSC::StartService ${SERVICE_NAME} "" 30 ;������ ������� ��� ���������� � ��������� � 30 ������
	        Pop $0
                ${If} $0 != 0
                      IfSilent +2
                         MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${SERVICE_NAME}. $\n ��������� ������ �������."
	        ${EndIf}
                WriteUninstaller "uninst_MBK07.exe"
	SectionEnd

;-------------------------------------------------------------------------------
; ������ ������ � ������������ ������� � �������, ����������� ������� ����������� psexec.
	Section ""
        	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Policies\System" "LocalAccountTokenFilterPolicy" 0x1
        	WriteRegStr HKCU "Software\Cometa\��� ���� ���" "IP_rm" "192.168.0.100"
        	WriteRegStr HKCU "Software\Cometa\��� ���� ���" "IP_gen" "192.168.1.224"

	SectionEnd


#-------------------------------------------------------------------------------
#
#         Uninstaller
#
#-------------------------------------------------------------------------------
	Var ProgmanHwnd
	Var ShellHwnd
	Var DesktopHwnd

	Function un.RefreshDesktop
	; 256=WM_KEYDOWN
	; 257=WM_KEYUP
	; 116=VK_F5
		FindWindow $ProgmanHwnd "Progman" "Program Manager"
		FindWindow $ShellHwnd "SHELLDLL_DefView" "" $ProgmanHwnd 0
		FindWindow $DesktopHwnd "SysListView32" "" $ShellHwnd 0
		System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 256, i 116, i 0)"
		System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 257, i 116, i 0x80000000)"
	FunctionEnd

        UninstallText "�������� ������� ��� ���07"
	UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\orange-uninstall.ico"

	Section "Uninstall"
	        DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\��� ���� ���"
		call un.FindAndDestroyService
		
		Delete "$INSTDIR\*.*"
		Delete "$INSTDIR\uninst_MBK07.exe"
		Delete /REBOOTOK "$SMPROGRAMS\��� ���� ���\${INSTDIR}\*.*"
		RMDir /r "$SMPROGRAMS\��� ���� ���\${INSTDIR}"

		Delete "$DESKTOP\������ ${INSTDIR} (������ �������).lnk"
		Delete "$DESKTOP\������ ${INSTDIR} ���������.lnk"
		
		call un.RefreshDesktop
		RMDir /r "$INSTDIR"

		IfFileExists "$INSTDIR\*.*" 0 UnNoErrorMsg
			MessageBox MB_OK "��������: ����� $INSTDIR �� ������� ���������!!!" IDOK 0
			
                UnNoErrorMsg:
	SectionEnd
