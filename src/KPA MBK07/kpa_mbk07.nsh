!IFNDEF KPA_MBK07_NSH
!define kpa_MBK07_nsh

;==========================================================================
; ����������� ��������������� �������

  !include "MUI2.nsh"                ;����� �������� ���������
  !include LogicLib.nsh              ;����������� �������������
  !include nsDialogs.nsh             ;�������� ����������� ����
  !include Plugins\include\ValidateIPAdress.nsh      ;�������� ������������ ����� IP-������

;==========================================================================
;���� �������
;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� ����� MBK07

  Function EnDisableMBK07Box
	Pop $CheckBoxMBK07
	${NSD_GetState} $CheckBoxMBK07 $0
	${If} $0 == 0
		EnableWindow $IP_MBK07 1
		EnableWindow $MBK07_Usr 1
		EnableWindow $MBK07_Pswd 1
		call MBK07IPAddrTextChange
	${Else}
		EnableWindow $IP_MBK07 0
		EnableWindow $MBK07_Usr 0
		EnableWindow $MBK07_Pswd 0
		call MBK07IPAddrTextChange
	${EndIf}
  FunctionEnd

;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� ����� ���3

  Function EnDisableSrv3Box
	Pop $CheckBoxSrv3
	${NSD_GetState} $CheckBoxSrv3 $0
	${If} $0 == 0
		EnableWindow $IP_Srv3 1
		EnableWindow $Srv3_Usr 1
		EnableWindow $Srv3_Pswd 1
		call Srv3IPAddrTextChange
	${Else}
		EnableWindow $IP_Srv3 0
		EnableWindow $Srv3_Usr 0
		EnableWindow $Srv3_Pswd 0
		call Srv3IPAddrTextChange
	${EndIf}
  FunctionEnd

;--------------------------------------------------------------------------
;������� ����������� ��������� MBK07 �������

  Function OnOffMBK07Box
	Pop $CheckBoxOff07
	${NSD_GetState} $CheckBoxOff07 $0
	${If} $0 == 0
		EnableWindow $IP_MBK07 1
		EnableWindow $MBK07_Usr 1
		EnableWindow $MBK07_Pswd 1
		EnableWindow $CheckBoxMBK07 1
	    call MBK07IPAddrTextChange
	${Else}
		EnableWindow $IP_MBK07 0
		EnableWindow $MBK07_Usr 0
		EnableWindow $MBK07_Pswd 0
		EnableWindow $CheckBoxMBK07 0
		call Srv3IPAddrTextChange
	${EndIf}

  FunctionEnd

;--------------------------------------------------------------------------
;������� ����������� ��������� Srv3 �������

  Function OnOffSrv3Box
	Pop $CheckBoxOff3
	${NSD_GetState} $CheckBoxOff3 $0
	${If} $0 == 0
		EnableWindow $IP_Srv3 1
		EnableWindow $Srv3_Usr 1
		EnableWindow $Srv3_Pswd 1
		EnableWindow $CheckBoxSrv3 1
		call Srv3IPAddrTextChange
	${Else}
		EnableWindow $IP_Srv3 0
		EnableWindow $Srv3_Usr 0
		EnableWindow $Srv3_Pswd 0
		EnableWindow $CheckBoxSrv3 0
		call Srv3IPAddrTextChange
	${EndIf}

  FunctionEnd


;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function MBK07IPAddrTextChange

        ClearErrors
        Pop $1 # $1 == $IP_MBK07

	${NSD_GetText} $IP_MBK07 $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $MBK07_Err "1"                 #
        ${Else}
              StrCpy $MBK07_Err "0"
        ${EndIf}                                  #

        Call EnDisableMBK07NextButton

  FunctionEnd

;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function Srv3IPAddrTextChange

        ClearErrors
        Pop $1 # $1 == $IP_Srv3

	    ${NSD_GetText} $IP_Srv3 $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $Srv3_Err "1"                 #
        ${Else}
              StrCpy $Srv3_Err "0"
        ${EndIf}                                  #

        Call EnDisableMBK07NextButton

  FunctionEnd
  

  
  
;--------------------------------------------------------------------------
;�������, ������������� ������ "�����" ��� ��������� �������� IP ������

  Function EnDisableMBK07NextButton

        GetDlgItem $MBK07NextButton $HWNDPARENT 1

        Pop $CheckBoxMBK07
    	${NSD_GetState} $CheckBoxMBK07 $1
		Pop $CheckBoxSrv3
	    ${NSD_GetState} $CheckBoxSrv3 $2

        IntOp $1 $1 !
        IntOp $2 $2 !
        
        IntOp $Case5 $MBK07_Err && $1
        IntOp $Case6 $Srv3_Err && $2
        
        IntOp $Double_Err $Case5 || $Case6
        
        ${If} $Double_Err == 1
              EnableWindow $MBK07NextButton 0
;              MessageBox MB_OK "�� ������ 2-$Double_Err 5-$Case5 6-$Case6"
        ${Else}
              EnableWindow $MBK07NextButton 1
;              MessageBox MB_OK "�� ������ 2-$Double_Err 5-$Case5 6-$Case6"
        ${EndIf}
  FunctionEnd
  
;--------------------------------------------------------------------------
;�������, ��������� ��������

  Function MBK07PageCreate

        SectionGetFlags ${MBK07} $R0
        Pop $R0
        ${If} $R0 == 1
              goto show
        ${EndIf}
        abort
        show:

	nsDialogs::Create 1018        ;������� ��������, � "���������" � �����
	Pop $MBK07Dialog                   ;����������� ��� � ����������

	${If} $MBK07Dialog == error
              MessageBox MB_OK "�������� �������� ��� ���������. ���������� � ������������. $\n $MBK07Dialog"
	      Abort
	${EndIf}

	${NSD_CreateLabel} 15% 0% 100% 18% "����� ������������ ��������� �������� ���������. $\n\
                                         �������� �������� ���������: �������� ��� ���������.$\n\
                                         ��� ������������� ������� IP-����� ��������(��) �����(�)."
	Pop $MBK07Label

;=================================================================================================================
	${NSD_CreateCheckBox} 2% 27% 42% 13% "�������� ��� ��������� ���������� ������� �� ���07."
        Pop $CheckBoxMBK07
	    ${NSD_OnClick} $CheckBoxMBK07 EnDisableMBK07Box
	    
	${NSD_CreateCheckBox} 53%% 27% 42% 13% "��������, ��� ��������� ���������� �������� �������."
        Pop $CheckBoxSrv3
	    ${NSD_OnClick} $CheckBoxSrv3 EnDisableSrv3Box
/*******************************************     GROUP BOXES    **************************************************/
        ${NSD_CreateGroupBox} 0% 19% 49% 70% "�������� ��������� �� �� ���07"
        Pop $MBK07GBox
        ${NSD_CreateGroupBox} 50% 19% 50% 70% "�������� ��������� �������� �������"
        Pop $GBox
/*******************************************     IP ��� 07   ****************************************************/
	${NSD_CreateLabel} 2% 45% 13% 9% "IP-�����:"
	Pop $MBK07Label

	${NSD_CreateText} 15% 44% 32% 9% $IP_MBK07_State
    Pop $IP_MBK07
/*******************************************     IP 3-�� �������   ****************************************************/
    ${NSD_CreateLabel} 53% 45% 13% 9% "IP-�����:"
	Pop $Label

	${NSD_CreateText} 67% 44% 31% 9% $IP_Srv3_State
        Pop $IP_Srv3
/*******************************************  ����� �� ��������  **************************************************/
	${NSD_CreateLabel} 2% 57% 13% 9% "�����:"
	Pop $MBK07Label

        ${NSD_CreateText} 15% 55% 32% 9% $MBK07_Usr_State
        Pop $MBK07_Usr
/*******************************************  ����� �� 3-� �������  **************************************************/
	${NSD_CreateLabel} 53% 57% 13% 9% "�����:"
	Pop $Label

        ${NSD_CreateText} 67% 55% 31% 9% $Srv3_Usr_State
        Pop $Srv3_Usr
/*******************************************  ������ �� ��������  **************************************************/
        ${NSD_CreateLabel} 2% 67% 13% 9% "������:"
	Pop $MBK07Label

        ${NSD_CreateText} 15% 66% 32% 9% $MBK07_Pswd_State
        Pop $MBK07_Pswd
/*******************************************  ������ �� �������3  **************************************************/
        ${NSD_CreateLabel} 53% 67% 13% 9% "������:"
	Pop $Label

        ${NSD_CreateText} 67% 66% 31% 9% $Srv3_Pswd_State
        Pop $Srv3_Pswd
/******************************************************************************************************************/
	${NSD_CreateCheckBox} 2% 74% 42% 13% "�� ������������� ������ ���07."
        Pop $CheckBoxOff07
	    ${NSD_OnClick} $CheckBoxOff07 OnOffMBK07Box ;TODO ������� ����� ����� �������� EnDisableBox

	${NSD_CreateCheckBox} 53%% 74% 42% 13% "�� ������������� 3-� ������."
        Pop $CheckBoxOff3
	    ${NSD_OnClick} $CheckBoxOff3 OnOffSrv3Box ;TODO ������� ����� ����� �������� EnDisableBox
;=====================================================================================================================

        ; �� ��������� IP-������ ���������
        EnableWindow $IP_MBK07 1
        EnableWindow $MBK07_Usr 1
        EnableWindow $MBK07_Pswd 1

        EnableWindow $IP_Srv3 1
        EnableWindow $Srv3_Usr 1
        EnableWindow $Srv3_Pswd 1
        
        ${NSD_OnChange} $IP_MBK07 MBK07IPAddrTextChange
        ${NSD_OnChange} $IP_Srv3 Srv3IPAddrTextChange
        
/***************************************/
        ${If} $CheckBoxMBK07_State == ${BST_CHECKED}
        ${AndIf} $CheckBoxOff07_State != ${BST_CHECKED}
		${NSD_Check} $CheckBoxMBK07
                EnableWindow $IP_MBK07 0
                EnableWindow $MBK07_Usr 0
                EnableWindow $MBK07_Pswd 0
	    ${EndIf}
	    
        ${If} $CheckBoxSrv3_State == ${BST_CHECKED}
        ${AndIf} $CheckBoxOff3_State != ${BST_CHECKED}
		      ${NSD_Check} $CheckBoxSrv3
		      EnableWindow $IP_Srv3 0
		      EnableWindow $Srv3_Usr 0
		      EnableWindow $Srv3_Pswd 0
	    ${EndIf}
;----------------------------------------
        ${If} $CheckBoxOff07_State == ${BST_CHECKED}
       		${NSD_Check} $CheckBoxOff07
                EnableWindow $IP_MBK07 0
                EnableWindow $MBK07_Usr 0
                EnableWindow $MBK07_Pswd 0
                EnableWindow $CheckBoxSrv07 0
	    ${EndIf}

        ${If} $CheckBoxOff3_State == ${BST_CHECKED}
       		${NSD_Check} $CheckBoxOff3
                EnableWindow $IP_Srv3 0
                EnableWindow $Srv3_Usr 0
                EnableWindow $Srv3_Pswd 0
                EnableWindow $CheckBoxSrv3 0
	    ${EndIf}

      	call MBK07IPAddrTextChange
        call Srv3IPAddrTextChange
        nsDialogs::Show

  FunctionEnd
;--------------------------------------------------------------------------
;�������, �������������� ��� ����� �� ��������
  Function MBK07PageLeave

        ${NSD_GetState} $CheckBoxMBK07 $CheckBoxMBK07_State
        ${If} $CheckBoxMBK07_State != ${BST_CHECKED}

        ${Else}
                StrCpy $IP_MBK07_State "192.168.1.13"
                ${NSD_SetText} $IP_MBK07 $IP_MBK07_State
	${EndIf}


        ${NSD_GetText} $IP_MBK07 $IP_MBK07_State
        ${NSD_GetText} $IP_Srv3 $IP_Srv3_State
        
        ${NSD_GetText} $MBK07_Usr $MBK07_Usr_State
        ${NSD_GetText} $MBK07_Pswd $MBK07_Pswd_State

        ${NSD_GetText} $Srv3_Usr $Srv1_Usr_State
        ${NSD_GetText} $Srv3_Pswd $Srv1_Pswd_State

        ${NSD_GetState} $CheckBoxMBK07 $CheckBoxMBK07_State
        ${NSD_GetState} $CheckBoxSrv3 $CheckBoxSrv3_State
        
        ${NSD_GetState} $CheckBoxOff07 $CheckBoxOff07_State
        ${NSD_GetState} $CheckBoxOff3  $CheckBoxOff3_State
        
        ;MessageBox MB_OK "V(^__^)V $CheckBoxOff07_State $CheckBoxOff3_State"
        ;MessageBox MB_OK "(V)(^,,,^)(V)$\n $\n   \(0o0)/ $\n $Srv_usr_State $\n $Srv_pswd_State $\n $MBK07_usr_State $\n $MBK07_pswd_State"

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_MBK07" $IP_MBK07_State

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "MBK07_usr" $MBK07_Usr_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "MBK07_pswd" $MBK07_Pswd_State

        StrCpy $MBK07_Err "0"
        DetailPrint "����� ��� ���: $IP_MBK07_State"
        ;����-��������
        ;MessageBox MB_OK "V(^__^)V $\n����� �������: $IP_srv_State$\n$\n����� ��������: $IP_MBK07_State$\n$\n"
  FunctionEnd
;==========================================================================
;MessageBox MB_OK "V(^__^)V" - ����-��������
;MessageBox MB_OK "(V)(^,,,^)(V)" - �����-������-��������
;MessageBox MB_OK "\(0o0)/ - ���� ��������"

;MessageBox MB_OK  (\.../) (\.../) (\.../) (\.../) $\n ( *.*) ('.'= ) ('.'= ) ('.'= ) $\n (")_(") (")_(") (")_(") (")_(") $\n ��������� ��������-����������"
!endif