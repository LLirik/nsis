!ifndef MONITOR_NSH
!define MONITOR_NSH

;==========================================================================
; ����������� ��������������� �������

  !include "MUI2.nsh"                  ;����� �������� ���������
  !include LogicLib.nsh              ;����������� �������������
  !include nsDialogs.nsh             ;�������� ����������� ����
  !include Plugins\include\ValidateIPAdress.nsh      ;�������� ������������ ����� IP-������
  

;==========================================================================
;���� �������


/*
  TODO ����������� ���������� ��� ������ ����� �������.
       ��� �����, ������ ����������� �������� � ���� ��� � "���������"
       �� TextBox
  end TODO
*/
;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� ��������

  Function EnDisableMonBox
	Pop $CheckBoxMon
	${NSD_GetState} $CheckBoxMon $0
	${If} $0 == 0
		EnableWindow $IP_Mon 1
		EnableWindow $Mon_Usr 1
		EnableWindow $Mon_Pswd 1
		call MonIPAddrTextChange
	${Else}
		EnableWindow $IP_Mon 0
		EnableWindow $Mon_Usr 0
		EnableWindow $Mon_Pswd 0
		call MonIPAddrTextChange
	${EndIf}
  FunctionEnd
  
;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������ �������
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function MonIPAddrTextChange

        ClearErrors
        Pop $1 # $1 == $IP_mon
        
	${NSD_GetText} $IP_Mon $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $Mon_Err "1"                 #
        ${Else}
              StrCpy $Mon_Err "0"
        ${EndIf}                                  #

        Call EnDisableMonNextButton
        
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ������������� ������ "�����" ��� ��������� �������� IP ������

  Function EnDisableMonNextButton

        GetDlgItem $MonNextButton $HWNDPARENT 1

        Pop $CheckBoxMon
	${NSD_GetState} $CheckBoxMon $1

        IntOp $1 $1 !
        IntOp $Case3 $Mon_Err && $1
        
        ${If} $Case3  == 1
              EnableWindow $MonNextButton 0
        ${Else}
              EnableWindow $MonNextButton 1
              ;MessageBox MB_OK "�� ������"
        ${EndIf}
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ��������� ��������

  Function MonitorPageCreate
  
        SectionGetFlags ${Monitor} $R0
        Pop $R0
        ${If} $R0 == 1
              goto show
        ${EndIf}
        abort
        show:
  
	nsDialogs::Create 1018        ;������� ��������, � "���������" � �����
	Pop $MonDialog                   ;����������� ��� � ����������

	${If} $MonDialog == error
              MessageBox MB_OK "�������� �������� ��� ���������. ���������� � ������������. $\n $MonDialog"
	      Abort
	${EndIf}

	${NSD_CreateLabel} 15% 0% 100% 18% "����� ������������ ��������� �������� ���������. $\n\
                                         �������� �������� ���������: �������� ��� ���������.$\n\
                                         ��� ������������� ������� IP-����� ��������(��) �����(�)."
	Pop $MonLabel
;=================================================================================================================
	${NSD_CreateCheckBox} 10%% 27% 92% 13% "�������� ��� ��������� ���������� ��������."
        Pop $CheckBoxMon
	    ${NSD_OnClick} $CheckBoxMon EnDisableMonBox ;TODO ������� ����� ����� �������� EnDisableBox
/*******************************************     GROUP BOXES    **************************************************/
        ${NSD_CreateGroupBox} 0% 19% 100% 70% "�������� ��������� ��������"
        Pop $MonGBox
/*******************************************     IP ��������   ****************************************************/
	${NSD_CreateLabel} 10% 45% 13% 9% "IP-�����:"
	Pop $MonLabel

	${NSD_CreateText} 27% 44% 60% 9% $IP_Mon_State
        Pop $IP_Mon
/*******************************************  ����� �� ��������  **************************************************/
	${NSD_CreateLabel} 10% 57% 13% 9% "�����:"
	Pop $MonLabel

        ${NSD_CreateText} 27% 55% 60% 9% $Mon_Usr_State
        Pop $Mon_Usr
/*******************************************  ������ �� ��������  **************************************************/
        ${NSD_CreateLabel} 10% 67% 13% 9% "������:"
	Pop $MonLabel

        ${NSD_CreateText} 27% 66% 60% 9% $Mon_Pswd_State
        Pop $Mon_Pswd
;=====================================================================================================================

        ; �� ��������� IP-������ ���������
        EnableWindow $IP_Mon 1
        EnableWindow $Mon_Usr 1
        EnableWindow $Mon_Pswd 1

        ${NSD_OnChange} $IP_Mon MonIPAddrTextChange
        
        ${If} $CheckBoxMon_State == ${BST_CHECKED}
		${NSD_Check} $CheckBoxMon
                EnableWindow $IP_Mon 0
                EnableWindow $Mon_Usr 0
                EnableWindow $Mon_Pswd 0
	${EndIf}
	
	call MonIPAddrTextChange

	nsDialogs::Show

  FunctionEnd
;--------------------------------------------------------------------------
;�������, �������������� ��� ����� �� ��������
  Function MonitorPageLeave
  
        ${NSD_GetState} $CheckBoxMon $CheckBoxMon_State
        ${If} $CheckBoxMon_State != ${BST_CHECKED}

        ${Else}
                StrCpy $IP_Mon_State "192.168.1.11"
                ${NSD_SetText} $IP_Mon $IP_Mon_State
	${EndIf}

	
        ${NSD_GetText} $IP_Mon $IP_Mon_State
        
        ${NSD_GetText} $Mon_Usr $Mon_Usr_State
        ${NSD_GetText} $Mon_Pswd $Mon_Pswd_State
        
        ${NSD_GetState} $CheckBoxMon $CheckBoxMon_State

        ;MessageBox MB_OK "(V)(^,,,^)(V)$\n $\n   \(0o0)/ $\n $Srv_usr_State $\n $Srv_pswd_State $\n $Mon_usr_State $\n $Mon_pswd_State"

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_mon" $IP_Mon_State

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Mon_usr" $Mon_Usr_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Mon_pswd" $Mon_Pswd_State
        
        StrCpy $Mon_Err "0"
        DetailPrint "����� ��������: $IP_Mon_State"
        ;����-�������� 
        ;MessageBox MB_OK "V(^__^)V $\n����� �������: $IP_srv_State$\n$\n����� ��������: $IP_mon_State$\n$\n"
  FunctionEnd
;==========================================================================
;MessageBox MB_OK "V(^__^)V" - ����-��������
;MessageBox MB_OK "(V)(^,,,^)(V)" - �����-������-��������
;MessageBox MB_OK "\(0o0)/ - ���� ��������"

;MessageBox MB_OK  (\.../) (\.../) (\.../) (\.../) $\n ( *.*) ('.'= ) ('.'= ) ('.'= ) $\n (")_(") (")_(") (")_(") (")_(") $\n ��������� ��������-����������"
!endif