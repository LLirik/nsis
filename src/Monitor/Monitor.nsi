#-------monitor.nsi-------------------------------------------------------------
#
#       Incudes
#
#-------------------------------------------------------------------------------

        !include LogicLib.nsh
        !include FileFunc.nsh
        !insertmacro Locate

 	!define PROJECT_ROOT "..\.."
        !ifndef TARGET
        !define TARGET "target_template"
        !endif
        !define TARGET_ROOT "${PROJECT_ROOT}\${TARGET}\�������"
	!define SERVICE_NAME "QCOMMonitor"
	!define INSTDIR "�������"
	!define COMAPP_NAME "�ommon"
	!define COMAPP_GUID "{166A651B-913C-4509-A9A4-8B9D750963FD}"
	InstallDir "$PROGRAMFILES64\Cometa\��� ���� ���\${INSTDIR}"
	
	!addplugindir "Plugins\bin"
	!addplugindir "${PROJECT_ROOT}\plugins\bin"
#-------------------------------------------------------------------------------
#
#       General options
#
#-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;Name and file_name
        Name "��� ���� ���"
	OutFile "${PROJECT_ROOT}\monitor.exe"
	
;-------------------------------------------------------------------------------
;Visual information
	SetDatablockOptimize on
	XPStyle on
	AutoCloseWindow false
	ShowInstDetails show
        SetCompressor lzma
        
	BrandingText "��� $\"���������� ������$\" ����-44$\n2012-2015"
	LoadLanguageFile "${NSISDIR}\Contrib\Language files\Russian.nlf"
	
;-------------------------------------------------------------------------------
;������� ����� �������������� �� ���������

	RequestExecutionLevel admin
#-------------------------------------------------------------------------------
#
#       Pages
#
#-------------------------------------------------------------------------------

	Page components
	Page directory
	Page instfiles

	UninstPage uninstConfirm
	UninstPage instfiles

#-------------------------------------------------------------------------------
	
	Var /GLOBAL switch_overwrite
	!include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"

#-------------------------------------------------------------------------------
#
#       Functions
#
#-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;����������� � ��������� (���� ����������) ������


	!macro FindAndDestroyServiceMACRO un
		Function "${un}FindAndDestroyService"
			ClearErrors
			DetailPrint "����� ��������� ${SERVICE_NAME}"
			SimpleSC::GetServiceStatus ${SERVICE_NAME}
			Pop $0 ; ���������� ��� ������ ���� ������� �� ����������: (!=0) � ��������� ������ ����� �����: (0)
			Pop $1 ; ���������� ������ ������� (�� ����:)
			DetailPrint "��� ���������:$\n ��� ������: $0 $\n ������ ������ ${SERVICE_NAME}: $1"
			#MessageBox MB_OK|MB_ICONEXCLAMATION "��� ���������:$\n ��� ������: $0 $\n ������ ������: $1"
			/*  1 - STOPPED
				2 - START_PENDING
				3 - STOP_PENDING
				4 - RUNNING
				5 - CONTINUE_PENDING
				6 - PAUSE_PENDING
				7 - PAUSED
			*/
			${If} $0 == 0      ; ���� ������ ����, ����� ��������� � ������
				${If} $1 == 4
					DetailPrint "${SERVICE_NAME} ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"
					#MessageBox MB_OK|MB_ICONEXCLAMATION "${SERVICE_NAME} ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"

					SimpleSC::StopService ${SERVICE_NAME} 1 30 ; ����� ����� ����������� ���� � ������� 30 ������.
					Pop $R0
					########################################################
          ExecWait "taskkill /F /IM ${COMAPP_NAME}.exe" #���� ����� ���-�� ������ - ���� ���� ����� � ������!!!
					StrCmp $R0 0 dead_meat why_wont_you_die ;��������� ��������� ��� �������� �������� � ������ �� �� �������!?
					why_wont_you_die:
						MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${SERVICE_NAME}. ��������� ������� �������,$\n����� ���� ������� ��."
					dead_meat:
						DetailPrint  "������ ${SERVICE_NAME} ������� ����������� � ����� �������!"
				${Else}
					DetailPrint  "������ ${SERVICE_NAME} �� ���� �������� � ����� �������!"
				${EndIf}
			${EndIf}
			Pop $R1

			${If} $R1 == 1
				ExecWait "sc delete ${SERVICE_NAME}"
			${EndIf}
			ExecWait "taskkill /F /IM ${COMAPP_NAME}.exe" #���� ����� ���-�� ������, ��� �� ������ - �� ����� �����
		FunctionEnd
	!macroend

	!insertmacro FindAndDestroyServiceMACRO ""
	!insertmacro FindAndDestroyServiceMACRO "un."

;-------------------------------------------------------------------------------
;�������� ������� ��� ��������

	Function "MonitorShortCut"
	
		CreateDirectory "$SMPROGRAMS\��� ���� ���\${INSTDIR}"

		CreateShortCut  "$SMPROGRAMS\��� ���� ���\${INSTDIR}\������� ${INSTDIR}.lnk"   "$INSTDIR\uninst_mon.exe"
		CreateShortCut "$DESKTOP\${INSTDIR} (������ �������).lnk" "$INSTDIR\${COMAPP_NAME}.exe" -i
		CreateShortCut "$DESKTOP\${INSTDIR} ���������.lnk" "$INSTDIR\${COMAPP_NAME}.exe" -e
		CreateShortCut "$SMPROGRAMS\��� ���� ���\${INSTDIR}.lnk" "$INSTDIR\${COMAPP_NAME}.exe" -i
	
	FunctionEnd
	
;-------------------------------------------------------------------------------
;��������� ���� ����������� ���������� � ���� ��� ������ ��������

	Function "SetMonitorPermissions"
		ExecWait "net user /expires:never /add mkpa mkpa"  ; ������ ������������ mkpa � ������� mkpa. ���� ��������� ������ �����������
		ExecWait "net localgroup �������������� mkpa /add" ; ��������� ������������ � ������ ���������������
			IfErrors 0 +2
		ExecWait "net localgroup Administrators mkpa /add"
		
		ExecWait "net user /expires:never /add ������� 1234"  ; ������ ������������ mkpa � ������� mkpa. ���� ��������� ������ �����������
		ExecWait "net localgroup �������������� ������� /add" ; ��������� ������������ � ������ ���������������
			IfErrors 0 +2
		ExecWait "net localgroup Administrators ������� /add"
		
		SetOutPath "$TEMP"
		FILE /r "${PROJECT_ROOT}\Vendor\dcomperm.exe"
		FILE /r "${PROJECT_ROOT}\Vendor\ntrights.exe"
		
		ExecWait 'sc delete ${SERVICE_NAME}'
		ExecWait 'sc create ${SERVICE_NAME} binPath= "$INSTDIR\${COMAPP_NAME}.exe"' ; ������ ������
		ExecWait "sc config ${SERVICE_NAME} obj= .\mkpa password= mkpa" ; ������ �� ����� ������������ ����
		ExecWait "$TEMP\dcomperm.exe -al ${COMAPP_GUID} set mkpa permit level:ll,rl,la,ra" ; ����� �� ������� ������ � ��.
		ExecWait "$TEMP\ntrights.exe +r SeServiceLogonRight -u mkpa" ; ���������� �� ������ � �������� ������
		ExecWait "$INSTDIR\${COMAPP_NAME}.exe /regserver" ; ����������� ��� �������
		
	        ExecWait "$INSTDIR\regtlibv12.exe $INSTDIR\${COMAPP_NAME}.tlb"
		
		FILE "${PROJECT_ROOT}\Vendor\Lsa_new.reg"
       	ExecWait "Regedit /s Lsa_new.reg" ; ��������� ������ � ������ � ������
        Delete $INSTDIR\Lsa_new.reg

		ExecWait "net start ${SERVICE_NAME}" ; ������ ������ (����� ������� ����� ���������)
        ExecWait "sc config ${SERVICE_NAME} start= auto"

	FunctionEnd

#-------------------------------------------------------------------------------
#
#        Sections
#
#-------------------------------------------------------------------------------
        Section ""
		Push 0
		Call FindAndDestroyService
		RMDir /r "$INSTDIR_backup"
		StrCpy $switch_overwrite 1
		!insertmacro MoveFolder "$INSTDIR" "$INSTDIR_backup" "*.*"
	SectionEnd


/*	Section "" save_old
                RmDir "$INSTDIR\backup_�������"
                Call FindAndDestroyService

                StrCpy $switch_overwrite 1 #0- ������������ ������ ������ ����� ������. 1- �� ������������
                !insertmacro MoveFolder "$INSTDIR\�������\" "$INSTDIR\backup_�������" "*.*"
                IfFileExists "$INSTDIR\backup_�������" 0 +2
                    ;MessageBox MB_OK "��������: ����� $INSTDIR\������ ������ ����!!!" IDOK 0
                    RmDir "$INSTDIR\backup_�������"
	SectionEnd
        */
;-------------------------------------------------------------------------------

    Section "OmniBusBox" OmniSec
		SetOutPath "$TEMP"
		FILE /r "${PROJECT_ROOT}\Vendor\OMNIBUSBOX\WINDOWS32"
		ExecWait "msiexec /package $TEMP\WINDOWS32\OMNIBUSBOX.MSI /quiet"
                ;RMDir /r "$TEMP\WINDOWS32"

		SetOutPath "$PROGRAMFILES\BUSBOX"
		FILE /a /r "${PROJECT_ROOT}\Vendor\BUSBOX\*.*"
		WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "OMNIBUS_HOME" '"$PROGRAMFILES\BUSBOX\API"'
		IfSilent +3
		MessageBox MB_YESNO|MB_ICONEXCLAMATION "������ ��������� �������������� � OmniBusBox? $\n������� '��' ���� ������� �������������� �������."  IDNO +2
                ExecWait '"$PROGRAMFILES\BUSBOX\TEST\BTITST32.EXE"'
	SectionEnd
	
;-------------------------------------------------------------------------------
	Function .onGUIEnd
                SectionGetFlags ${OmniSec} $0
                IntOp $0 $0 & 1
                StrCmp $0 0 0 +2
                Abort
                MessageBox MB_YESNO|MB_ICONEXCLAMATION "������ ��������� �������������� � OmniBusBox? $\n������� '��' ���� ������� �������������� ������ ������."  IDNO +2
                ExecWait '"$PROGRAMFILES\BUSBOX\TEST\BTITST32.EXE"'
        FunctionEnd
; Uninstaller

        Var ProgmanHwnd
        Var ShellHwnd
        Var DesktopHwnd

        Function un.RefreshDesktop

            ; 256=WM_KEYDOWN
            ; 257=WM_KEYUP
            ; 116=VK_F5
            FindWindow $ProgmanHwnd "Progman" "Program Manager"
            FindWindow $ShellHwnd "SHELLDLL_DefView" "" $ProgmanHwnd 0
            FindWindow $DesktopHwnd "SysListView32" "" $ShellHwnd 0
            System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 256, i 116, i 0)"
            System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 257, i 116, i 0x80000000)"

        FunctionEnd
	Section "������� ���" MoniSect
		SectionIn 1 2
		SectionSetFlags SF_SELECTED 1

		Push 1 #������� ������
		Call FindAndDestroyService
		
		SetOutPath "$TEMP"
		FILE /r "${PROJECT_ROOT}\Vendor\vcredist_x86.exe"
		ExecWait "$TEMP\vcredist_x86.exe /q"
		DELETE "$TEMP\vcredist_x86.exe"

		SetOutPath "$INSTDIR"
		
		FILE /r "${TARGET_ROOT}\*.*"

		FILE    "${PROJECT_ROOT}\Vendor\regtlibv12.exe"

 		Call SetMonitorPermissions
		Call MonitorShortCut
		
		ClearErrors
	        SimpleSC::StartService ${Service_Name} "" 30 ;������ ������� ��� ���������� � ��������� � 30 ������
	        Pop $0
                ${If} $0 != 0
                      IfSilent +2
                         MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${Service_Name}. ��������� ������ �������. ������=$0"
	        ${EndIf}
	        
	        WriteUninstaller "uninst_mon.exe"
	        
	SectionEnd
	
;-------------------------------------------------------------------------------
; ������ ������ � ������������ ������� � �������, ����������� ������� ����������� psexec.
	Section ""
        	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Policies\System" "LocalAccountTokenFilterPolicy" 0x1
	SectionEnd
	
;-------------------------------------------------------------------------------
;�� ���������� ��������� ���������� �������� BTITST
/*	Function .onGUIEnd
                SectionGetFlags ${OmniSect} $0
                IntOp $0 $0 & 1
                StrCmp $0 0 0 +2
                Abort
                MessageBox MB_YESNO|MB_ICONEXCLAMATION "������ ��������� �������������� � OmniBusBox? $\n������� '��' ���� ������� �������������� �������."  IDNO +2
                ExecWait '"$PROGRAMFILES\BUSBOX\TEST\BTITST32.EXE"'
        FunctionEnd
*/
#-------------------------------------------------------------------------------
#
#         Uninstaller
#
#-------------------------------------------------------------------------------

	UninstallText "�������� ��������"
	UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\orange-uninstall.ico"


	Section "Uninstall"

		DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\��� ���� ���"

		call un.FindAndDestroyService

		Delete "$INSTDIR\*.*"
		Delete "$INSTDIR\uninst_mon.exe"
		Delete /REBOOTOK "$SMPROGRAMS\��� ���� ���\${INSTDIR}\*.*"
		RMDir /r "$SMPROGRAMS\��� ���� ���\${INSTDIR}"
		
#		MessageBox MB_OK "��������: ����� -������� ����- - $DESKTOP"

		Delete "$DESKTOP\${INSTDIR} (������ �������).lnk"
		Delete "$DESKTOP\${INSTDIR} ���������.lnk"

#                ExecWait "taskkill /f /im explorer.exe & explorer.exe"
                call un.RefreshDesktop
		RMDir /r "$INSTDIR"

		IfFileExists "$INSTDIR\*.*" 0 UnNoErrorMsg
			MessageBox MB_OK "��������: ����� ${INSTDIR} �� ������� ���������!!!" IDOK 0

		UnNoErrorMsg:
	SectionEnd