	!macro FindAndDestroyServiceMACRO un
		Function "${un}FindAndDestroyService"
			ClearErrors
			DetailPrint "����� ��������� ${SERVICE_NAME}"
			SimpleSC::GetServiceStatus ${SERVICE_NAME}
			Pop $0 ; ���������� ��� ������ ���� ������� �� ����������: (!=0) � ��������� ������ ����� �����: (0)
			Pop $1 ; ���������� ������ ������� (�� ����:)
			DetailPrint "��� ���������:$\n ��� ������: $0 $\n ������ ������ ${SERVICE_NAME}: $1"
			#MessageBox MB_OK|MB_ICONEXCLAMATION "��� ���������:$\n ��� ������: $0 $\n ������ ������: $1"
			/*  1 - STOPPED
				2 - START_PENDING
				3 - STOP_PENDING
				4 - RUNNING
				5 - CONTINUE_PENDING
				6 - PAUSE_PENDING
				7 - PAUSED
			*/
			
			${If} $0 == 0      ; ���� ������ ����, ����� ��������� � ������
				${If} $1 == 4
					DetailPrint "${SERVICE_NAME} ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"
					#MessageBox MB_OK|MB_ICONEXCLAMATION "${SERVICE_NAME} ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"

					SimpleSC::StopService ${SERVICE_NAME} 1 30 ; ����� ����� ����������� ���� � ������� 30 ������.
					Pop $R0

					StrCmp $R0 0 dead_meat why_wont_you_die ;��������� ��������� ��� �������� �������� � ������ �� �� �������!?
					why_wont_you_die:
                                        /*
                                        :::::::::::
                                        :: ����� ��������� ������������� ���. ��� ��� �����������, ��� ������ �������� ��������� � ������ ����� �����!
                                        :::::::::::
                                        */
						ExecWait "taskkill /f /im ${COMAPP_NAME}.exe"
						ExecWait "taskkill /f /im crash_service.exe"
						;MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${SERVICE_NAME}. ��������� ������� �������,$\n����� ���� ������� ��."
					dead_meat:
						DetailPrint  "������ ${SERVICE_NAME} ������� ����������� � ����� �������!"
				${Else}
					DetailPrint  "������ ${SERVICE_NAME} �� ���� �������� � ����� �������!"	
				${EndIf}
			${EndIf}
			Pop $R1 

			${If} $R1 == 1
				ExecWait "sc delete ${SERVICE_NAME}"
			${EndIf}
		FunctionEnd
	!macroend
	 
	!insertmacro FindAndDestroyServiceMACRO ""
	!insertmacro FindAndDestroyServiceMACRO "un."	

	/*������� ������� ��� ������ ��� ��� ��� ����, � ������:
        �� ������� ����� ����� ������� ������� ��� ������ � ������ � ����
        � "��� ���������" �� �� ��� ������ + ����� ��������
        � ������������ ����������� ������ "����������� ��� ���������� �������� ��� ����*/
	Function "ServerShortCut"
		CreateDirectory "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}"
		CreateShortCut  "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}\������� ${INSTDIR}.lnk"   "$INSTDIR\uninst_server.exe"
		CreateShortCut  "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}\${INSTDIR}(������ �������).lnk" "$INSTDIR\${COMAPP_NAME}.exe" -i "$INSTDIR\server.ico"
		CreateShortCut  "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}\${INSTDIR} ���������.lnk" "$INSTDIR\${COMAPP_NAME}.exe" "-e" "$INSTDIR\server.ico"
		
		CreateShortCut  "$DESKTOP\${INSTDIR} (������ �������).lnk" "$INSTDIR\${COMAPP_NAME}.exe" -i "$INSTDIR\server.ico"
		CreateShortCut  "$DESKTOP\${INSTDIR} ���������.lnk" "$INSTDIR\${COMAPP_NAME}.exe" "-e" "$INSTDIR\server.ico"
		SetShellVarContext all
		CreateShortCut "$SMSTARTUP\����������.lnk" "$INSTDIR\restart.bat"
		SetShellVarContext current
	FunctionEnd
	
	/* ������� ������������� ��� ����������� ����� ��� ������ ���
	�� ����
        :::::::::::
        :: ���� � ���, ��� � ���� ������� �� �������������� ������� ������ �������,
        :: ������� ���������� ��� ������ � ��������������
        :::::::::::
        */
        
	Function "SetServerPermissions"
		ExecWait "net user /expires:never /add mkpa mkpa"
		ExecWait `WMIC USERACCOUNT WHERE "Name='mkpa'" SET PasswordExpires=FALSE`
		ExecWait "net localgroup �������������� mkpa /add"

		IfErrors 0 +2
			ExecWait "net localgroup Administrators mkpa /add"
		ClearErrors
        
		SetOutPath "$TEMP"
		FILE "${PROJECT_ROOT}\Vendor\dcomperm.exe"
		FILE "${PROJECT_ROOT}\Vendor\ntrights.exe"
        
		ExecWait "$INSTDIR\${COMAPP_NAME}.exe /regserver"
		
#TODO ���-�� ����������� ������ ����� �������� ��-��� ���������� �������.
#����� ����� ���� �� ����� ������������� ��� ������� (������� � ���07), � ����� �������
#ExecWait "$INSTDIR\common.exe /regserver"
#ExecWait "$INSTDIR\MBK07.exe /regserver"
#ExecWait "$INSTDIR\regtlibv12.exe $INSTDIR\comapp1.tlb"
#ExecWait "$INSTDIR\regtlibv12.exe $INSTDIR\common.tlb"
#ExecWait "$INSTDIR\regtlibv12.exe

		ExecWait 'sc create ${SERVICE_NAME} binPath= "$INSTDIR\${COMAPP_NAME}.exe"'
		ExecWait "sc config ${SERVICE_NAME} obj= .\mkpa password= mkpa"
		ExecWait '$TEMP\dcomperm.exe -al ${COMAPP_GUID} set mkpa permit level:ll,rl,la,ra'
		ExecWait '$TEMP\dcomperm.exe -al ${COMAPP_GUID} set "���" permit level:ll,rl,la,ra'
		ExecWait '$TEMP\dcomperm.exe -al ${COMAPP_GUID} set "REMOTE INTERACTIVE LOGON" permit level:ll,rl,la,ra'
		ExecWait '$TEMP\dcomperm.exe -al ${COMAPP_GUID} set "��������� ����" permit level:ll,rl,la,ra'
		
		ExecWait "$TEMP\ntrights.exe +r SeServiceLogonRight -u mkpa"
		   
		FILE "${PROJECT_ROOT}\Vendor\Lsa_new.reg"
		ExecWait "Regedit /s Lsa_new.reg"
		Delete "$INSTDIR\Lsa_new.reg"
		
		ExecWait "$INSTDIR\regtlibv12.exe $INSTDIR\${COMAPP_NAME}.tlb"		
		
		ExecWait "sc config ${SERVICE_NAME} start= auto"
	FunctionEnd
	