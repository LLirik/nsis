  !include MUI2.nsh
  !include LogicLib.nsh
  !include nsDialogs.nsh
  !include FileFunc.nsh
  !insertmacro Locate

  !define PROJECT_ROOT "..\.."
  !ifndef TARGET
  !define TARGET "target_template"
  !endif
  
  !define TARGET_ROOT "${PROJECT_ROOT}\${TARGET}\������"
  !include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"

	!addplugindir "Plugins\bin"
  #!include EnvVarUpdate.nsh
;--------------------------------------------------------------------------
;General

  ;Name and file
  Name "��� ���� ���"
  OutFile "${PROJECT_ROOT}\Client.exe"
  
  ;Default installation folder
  InstallDir "$PROGRAMFILES\Cometa\��� ���� ���"

  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\Cometa\��� ���� ���" ""

  ;Request application privileges for Windows Vista
  #RequestExecutionLevel admin
  SetCompressor /SOLID LZMA 
;==========================================================================
;���������� ����������

  Var /GLOBAL switch_overwrite

  Var ServiceName

  Var Dialog          ;���� ���� �������
  Var Label           ;���� ������� "������� IP-������...

  Var CheckBoxSrv1        ;������� "���� �������� ��������� ������� �������"
  Var CheckBoxSrv1_State  ;����������� ��������� ��������
  Var CheckBoxSrv2        ;������� "���� �������� ��������� ������� �������"
  Var CheckBoxSrv2_State  ;����������� ��������� ��������
  
  Var CheckBoxOff1        ;������� "���� �������� ��������� ��������"
  Var CheckBoxOff1_State  ;����������� ��������� ��������
  Var CheckBoxOff2        ;������� "���� �������� ��������� �������"
  Var CheckBoxOff2_State  ;����������� ��������� ��������

  Var IP_Srv1          ;IP-����� �������� 
  Var IP_Srv1_State    ;����������� ��������� ������ ��������
  Var IP_Srv2          ;IP-����� ������� 
  Var IP_Srv2_State    ;����������� ��������� ������ �������

  Var Srv1_Usr         ;���� ��� ��� ������������ �� �������
  Var Srv1_Usr_State   ;��� ������������
  Var Srv1_Pswd        ;���� ������
  Var Srv1_Pswd_State  ;������

  Var Srv2_Usr         ;���� ��� ��� ������������ �� �������
  Var Srv2_Usr_State   ;��� ������������
  Var Srv2_Pswd        ;���� ������
  Var Srv2_Pswd_State  ;������

  Var NextButton
  Var GBox

  Var Double_Err
  Var Srv2_Err
  Var Srv1_Err
  
  Var Case1
  Var Case2

;--------------------------------------------------------------------------

  Var MonDialog          ;���� ���� �������
  Var MonLabel           ;���� ������� "������� IP-������...

  Var CheckBoxMon        ;������� "���� �������� ��������� ��������"
  Var CheckBoxMon_State  ;����������� ��������� ��������

  Var IP_Mon          ;IP-����� ��������
  Var IP_Mon_State    ;����������� ��������� ������ ��������

  Var Mon_Usr         ;���� ��� ��� ������������ �� �������
  Var Mon_Usr_State   ;��� ������������
  Var Mon_Pswd        ;���� ������
  Var Mon_Pswd_State  ;������

  Var MonNextButton
  Var MonGBox

  Var Mon_Err
  Var Case3
;--------------------------------------------------------------------------

  Var ASNDialog          ;���� ���� �������
  Var ASNLabel           ;���� ������� "������� IP-������...

  Var CheckBoxASN        ;������� "���� �������� ��������� ��������"
  Var CheckBoxASN_State  ;����������� ��������� ��������

  Var IP_ASN          ;IP-����� ��������
  Var IP_ASN_State    ;����������� ��������� ������ ��������

  Var ASN_Usr         ;���� ��� ��� ������������ �� �������
  Var ASN_Usr_State   ;��� ������������
  Var ASN_Pswd        ;���� ������
  Var ASN_Pswd_State  ;������

  Var ASNNextButton
  Var ASNGBox

  Var ASN_Err
  Var Case4
;--------------------------------------------------------------------------

  Var MBK07Dialog          ;���� ���� �������
  Var MBK07Label           ;���� ������� "������� IP-������...

  Var CheckBoxMBK07        ;������� "���� �������� ��������� ��������"
  Var CheckBoxMBK07_State  ;����������� ��������� ��������
  
  Var CheckBoxSrv3        ;������� "���� �������� ��������� ��������"
  Var CheckBoxSrv3_State  ;����������� ��������� ��������

  Var IP_MBK07          ;IP-����� ��������
  Var IP_MBK07_State    ;����������� ��������� ������ ��������
  
  Var IP_Srv3          ;IP-����� ��������
  Var IP_Srv3_State    ;����������� ��������� ������ ��������

  Var CheckBoxOff07        ;������� "���� �������� ��������� ��������"
  Var CheckBoxOff07_State  ;����������� ��������� ��������
  Var CheckBoxOff3        ;������� "���� �������� ��������� �������"
  Var CheckBoxOff3_State  ;����������� ��������� ��������

  Var MBK07_Usr         ;���� ��� ��� ������������ �� �������
  Var MBK07_Usr_State   ;��� ������������
  Var MBK07_Pswd        ;���� ������
  Var MBK07_Pswd_State  ;������

  Var Srv3_Usr         ;���� ��� ��� ������������ �� �������
  Var Srv3_Usr_State   ;��� ������������
  Var Srv3_Pswd        ;���� ������
  Var Srv3_Pswd_State  ;������

  Var MBK07NextButton
  Var MBK07GBox

  Var MBK07_Err
  Var Srv3_Err
  Var Case5
  Var Case6
  
 ;--------------------------------------------------------------------------

  Var M4Dialog          ;���� ���� �������
  Var M4Label           ;���� ������� "������� IP-������...

  Var CheckBoxM4        ;������� "���� �������� ��������� ��������"
  Var CheckBoxM4_State  ;����������� ��������� ��������

  Var IP_M4          ;IP-����� ��������
  Var IP_M4_State    ;����������� ��������� ������ ��������

  Var M4_Usr         ;���� ��� ��� ������������ �� �������
  Var M4_Usr_State   ;��� ������������
  Var M4_Pswd        ;���� ������
  Var M4_Pswd_State  ;������

  Var M4NextButton
  Var M4GBox

  Var M4_Err
  Var Case7
;==========================================================================
;��������� ����������

  !define MUI_ABORTWARNING      ;�������� �������������� ��������� ��� ������ ���������
  !define MUI_ABORTWARNING_TEXT "�� ������ �������� ��������� ���?"
  ;!define MUI_ICON "E:\icon.ico" ;TODO
  !define MUI_HEADERIMAGE
#  !define MUI_HEADERIMAGE_BITMAP orange.bmp ; optional
#  !define MUI_WELCOMEFINISH_BITMAP arrow.bmp ; optional
  !define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install-colorful.ico"

  !define MUI_FINISHPAGE_NOAUTOCLOSE ;�� ��������� �������� � �������� ���������
;===========================================================================
;Pages

  !insertmacro MUI_PAGE_WELCOME
  ;!insertmacro MUI_PAGE_LICENSE "E:\LL\License.txt" ("E:\LL\ChangeLog.txt")
  !insertmacro MUI_PAGE_COMPONENTS
  
  Page custom ArchivePageCreate ArchivePageLeave
  /*
  Page custom ServersPageCreate ServersPageLeave
  Page custom MonitorPageCreate MonitorPageLeave
  Page custom ASNPageCreate  ASNPageLeave
  Page custom MBK07PageCreate  MBK07PageLeave
  Page custom KPAM4PageCreate  KPAM4PageLeave
  */
  !define MUI_PAGE_CUSTOMFUNCTION_PRE DirectoryPre
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
/*
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
*/
;===========================================================================
  !ifndef NOINSTTYPES
      InstType "���������� ����������"
	  InstType "�������������-������ ������"
	  InstType "������ ��� �������� ���"
	  InstType "������� ���"
	  InstType "����� ������������"
	  ;InstType /NOCUSTOM
	  ;InstType /COMPONENTSONLYONCUSTOM
  !endif
;===========================================================================
;����

  !insertmacro MUI_LANGUAGE "Russian"

;===========================================================================
;Installer Sections



;---------------------------------------------------------------------------
 Section ""
        WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\��� ���� ���" "DisplayName" "��� ���� ���(������ ��������)"
        WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\��� ���� ���" "UninstallString" '"$INSTDIR\������\uninst.exe"'

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_client_only" "$IP_Client_State" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_mon_only" "$IP_Mon_State" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_M4_only" "$IP_M4_State" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_ASN_only" "$IP_ASN_State" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv1_only" "$IP_Srv1_State" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv2_only" "$IP_Srv2_State" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_MBK07_only" "$IP_MBK07_State" #mkpa:mkpa@[IP]/
        
        WriteRegStr HKLM "Software\Cometa\��� ���� ���\Archive" "username" "mkpa" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���\Archive" "password" "mkpa" #mkpa:mkpa@[IP]/
        
        StrCpy $0 "/"
        StrCpy $1 "$Srv1_Usr_State:$Srv1_Pswd_State@$IP_Srv1_State"
        StrCpy $2 "$Srv2_Usr_State:$Srv2_Pswd_State@$IP_Srv2_State"
        StrCpy $3 "$Mon_Usr_State:$Mon_Pswd_State@$IP_Mon_State"
        StrCpy $4 "$ASN_Usr_State:$ASN_Pswd_State@$IP_ASN_State"
        StrCpy $5 "$MBK07_Usr_State:$MBK07_Pswd_State@$IP_MBK07_State"
        StrCpy $6 "$Srv3_Usr_State:$Srv3_Pswd_State@$IP_Srv3_State"
        StrCpy $7 "$M4_Usr_State:$M4_Pswd_State@$IP_M4_State"
        
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_mon" "$3$0" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_ASN" "$4$0" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_MBK07" "$5$0" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv1" "$1$0" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv2" "$2$0" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_srv3" "$6$0" #mkpa:mkpa@[IP]/
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_M4"   "$7$0" #mkpa:mkpa@[IP]/
        
        
        Call FindAndDestroy

 SectionEnd
 
;---------------------------------------------------------------------------

 Section "������" Client
 SectionIn 1 5
        SetAutoClose false
        
        CreateDirectory "$TEMP\mkpa_tmp"
        
        RmDir "$INSTDIR\backup_������"
        
	    CopyFiles "$INSTDIR\������\�����" "$TEMP\mkpa_tmp"
	    CopyFiles "$INSTDIR\������\processes" "$TEMP\mkpa_tmp"
		CopyFiles "$INSTDIR\������\��������" "$TEMP\mkpa_tmp"
        CopyFiles "$INSTDIR\������\values.vars" "$TEMP\values.vars"
       	CopyFiles "$PROGRAMFILES\MongoDB" "$TEMP\mkpa_tmp\MongoDB"

        CopyFiles "$INSTDIR\������\filter_settings.ini" "$TEMP\filter_settings.ini"
        StrCpy $switch_overwrite 1 #0- ������������ ������ ������ ����� ������. 1- �� ������������
        !insertmacro MoveFolder "$INSTDIR\������\" "$INSTDIR\backup_������" "*.*"
        IfFileExists "$INSTDIR\backup_������" 0 +2
;			MessageBox MB_OK "��������: ����� $INSTDIR\������ ������ ����!!!" IDOK 0
            RmDir "$INSTDIR\backup_������"
        ; Add an application to the firewall exception list - All Networks - All IP Version - Enabled
        ;SimpleFC::AddApplication "mongod" "$PROGRAMFILES\MongoDB\mongod.exe" 0 2 "" 1
        Pop $0 ; return error(1)/success(0)

        StrCmp $R0 0 added some_error
        some_error:
                   DetailPrint  "�� ������� �������� firewall windows. ��������� ��� �������."
                   #MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� $ServiceName. ��������� ������� �������."
        added:
                   DetailPrint  "Firewall ������� �������."

	RMDir /r "$INSTDIR\������"

	SetOutPath $INSTDIR
	
        FILE /r "${TARGET_ROOT}"
     
     
	   FileOpen $1 $INSTDIR\������\client_settings.ini w
	   FileWrite $1 "[main]$\r$\n"
       FileWrite $1 "first_srv_ip=$IP_Srv1_State$\r$\n"
       FileWrite $1 "second_srv_ip=$IP_Srv2_State$\r$\n"
       FileWrite $1 "third_srv_ip=$IP_Srv3_State$\r$\n"
       FileWrite $1 "monitor_ip=$IP_Mon_State$\r$\n"
;      FileWrite $1 "kpa_m_ch_ip=$IP_Srv2_State$\r$\n"
	   FileClose $1

	   CreateDirectory "$INSTDIR\������\��������"
       CreateDirectory "$INSTDIR\������\�����\��������� ���"

     
	   FILE /r "${PROJECT_ROOT}\Vendor\Lsa_new.reg"
	   ExecWait "Regedit /s Lsa_new.reg"
	   Delete "$INSTDIR\������\Lsa_new.reg"
	   Call ClientShortCut

	   ExecWait "net user /expires:never /add mkpa mkpa"
	   ExecWait "net localgroup �������������� mkpa /add"
	   IfErrors 0 +2
	            ExecWait "net localgroup Administrators mkpa /add"

       CreateDirectory "$PROGRAMFILES\������\MongoDB\data\db"

	   CopyFiles "$TEMP\mkpa_tmp\�����"     "$INSTDIR\������"
	   CopyFiles "$TEMP\mkpa_tmp\processes" "$INSTDIR\������"
	   CopyFiles "$TEMP\mkpa_tmp\��������" "$INSTDIR\������"
       CopyFiles "$TEMP\values.vars"        "$INSTDIR\������\values.vars"
       CopyFiles "$TEMP\filter_settings.ini" "$INSTDIR\������\filter_settings.ini"
	   CopyFiles "$TEMP\mkpa_tmp\MongoDB"   "$PROGRAMFILES"

#	RMDir /r "$TEMP\mkpa_tmp"
	   SetOutPath "$TEMP"
       FILE /r "${PROJECT_ROOT}\Vendor\vcredist_x86.exe"
	   ExecWait "$TEMP\vcredist_x86.exe /q"
/*
        FILE "server1.exe"
        File "${PROJECT_ROOT}\Vendor\regtlibv12.exe"
	    ExecWait "server1.exe /S"

        FILE "server2.exe"
	    ExecWait "server2.exe /S"

        FILE "server3.exe"
	    ExecWait "server3.exe /S"
	    
        FILE "monitor.exe"
	    ExecWait "monitor.exe /S"
	    
        FILE "kpa_mch.exe"
	    ExecWait "kpa_mch.exe /S"
*/
;-----------

/*
       File "�� ����\������ 1\comapp1.exe"
       File "�� ����\������ 2\comapp2.exe"
       File "�� ����\������ 3\comapp3.exe"
       File "�� ����\�������\common.exe"
       File "�� ����\���-��\kpa_mch2.exe"

       File "�� ����\��������\*.dll"
       File "�� ����\������ 1\logging.dll"
       File "�� ����\������ 1\CBK_pkodll.dll"
       File "�� ����\������ 1\CBKFlash.dll"
       File "�� ����\������ 1\QtCored4.dll"
       File "�� ����\������ 1\QtGuid4.dll"
       FILE "${TARGET_ROOT}\msvcr100d.dll"
       FILE "${TARGET_ROOT}\msvcp100d.dll"
	   FILE "�� ����\������ 1\QtNetwork4.dll"
  	   FILE "�� ����\������ 1\QtNetworkd4.dll"

       File "�� ����\������ 3\comapp3.tlb"
	   File "�� ����\������ 2\comapp2.tlb"
	   File "�� ����\������ 1\comapp1.tlb"
	
       ExecWait "$TEMP\comapp1.exe /regserver"
       ExecWait "$TEMP\comapp2.exe /regserver"
       ExecWait "$TEMP\comapp3.exe /regserver"
       ExecWait "$TEMP\common.exe  /regserver"
       ExecWait "$TEMP\kpa_mch2.exe /regserver"
       
       
       ExecWait "$TEMP\comapp1.exe -i"
       ExecWait "$TEMP\comapp2.exe -i"
       ExecWait "$TEMP\comapp3.exe -i"
       ExecWait "$TEMP\common.exe -i"
       ;ExecWait "$TEMP\kpa_mch2.exe -i"

       File "${PROJECT_ROOT}\Vendor\regtlibv12.exe"
       ExecWait "$TEMP\regtlibv12.exe $INSTDIR\������\comapp1.tlb"
       ExecWait "$TEMP\regtlibv12.exe $INSTDIR\������\comapp2.tlb"
       ExecWait "$TEMP\regtlibv12.exe $INSTDIR\������\comapp3.tlb"
       ExecWait "$TEMP\regtlibv12.exe $INSTDIR\������\common.tlb"
       ExecWait "$TEMP\regtlibv12.exe $INSTDIR\������\kpa_mch2.tlb"
       ExecWait "$TEMP\regtlibv12.exe $INSTDIR\������\client.tlb"
       
        ExecWait "$INSTDIR\������ 1\uninst_server1.exe /S"
        ExecWait "$INSTDIR\������ 2\uninst_server2.exe /S"
        ExecWait "$INSTDIR\������ 3\uninst_server3.exe /S"
        ExecWait "$INSTDIR\�������\uninst_mon.exe /S"
        ExecWait "$INSTDIR\���-��\uninst_kpa_mch.exe /S"
	*/
 SectionEnd
 /*
;---------------------------------------------------------------------------
 Section "�������" Servers
        SectionIn 2 3 5
        SetAutoClose false
        SetRebootFlag true
        
        SetOutPath "$TEMP"
        FILE "${PROJECT_ROOT}\Vendor\PsExec.exe"

        ${If} $CheckBoxOff1_State != ${BST_CHECKED}
             ${If} $CheckBoxSrv1_State != ${BST_CHECKED}
                      StrCpy $0 "/"
                      StrCpy $3 "$Mon_Usr_State:$Mon_Pswd_State@$IP_Mon_State"
                      StrCpy $4 "$ASN_Usr_State:$ASN_Pswd_State@$IP_ASN_State"
                      StrCpy $5 "$MBK07_Usr_State:$MBK07_Pswd_State@$IP_MBK07_State"
                      
                      FILE "server1.exe"

                      ClearErrors
                      ExecWait "$TEMP\psexec \\$IP_Srv1_State /accepteula -u $Srv1_Usr_State -p $Srv1_Pswd_State -c -h -f server1.exe cmd server1.exe /S"
                      IfErrors 0 +2
                               MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� ������� �������. $\n���������� ���������� ������ ������� �� ������ � �������:$\n   $IP_Srv1_State"

                      ClearErrors
                      ExecWait '$TEMP\psexec \\$IP_Srv1_State /accepteula -u $Srv1_Usr_State -p $Srv1_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v IP_mon /d $3$0 /f'
                      IfErrors 0 +2
                               MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� � ������ 1-�� �������:$\n  IP_mon == $3$0"

                      ExecWait '$TEMP\psexec \\$IP_Srv1_State /accepteula -u $Srv1_Usr_State -p $Srv1_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v ag_addr /d 192.168.1.105 /f'
                      ExecWait '$TEMP\psexec \\$IP_Srv1_State /accepteula -u $Srv1_Usr_State -p $Srv1_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v ag_port /d 5025 /f'
                      ExecWait '$TEMP\psexec \\$IP_Srv1_State /accepteula -u $Srv1_Usr_State -p $Srv1_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v IP_mbk07 /d $5$0 /f'
                      ExecWait '$TEMP\psexec \\$IP_Srv1_State /accepteula -u $Srv1_Usr_State -p $Srv1_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v IP_ASN /d $4$0 /f'


              ${Else}
 	             FILE "server1.exe"
 	             ExecWait "server1.exe"
# 	             ExecWait "server1.exe"
	          ${EndIf}
        ${EndIf} #


        ${If} $CheckBoxOff2_State != ${BST_CHECKED}
              ${If}   $CheckBoxSrv2_State != ${BST_CHECKED}
                      StrCpy $0 "/"
                      StrCpy $3 "$Mon_Usr_State:$Mon_Pswd_State@$IP_Mon_State"
                      StrCpy $4 "$ASN_Usr_State:$ASN_Pswd_State@$IP_ASN_State"
                      StrCpy $5 "$MBK07_Usr_State:$MBK07_Pswd_State@$IP_MBK07_State"
                      FILE "server2.exe"
					  
                      ExecWait '$TEMP\psexec \\$IP_Srv2_State /accepteula -u $Srv2_Usr_State -p $Srv2_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v IP_mon /d $3$0 /f'
                      ClearErrors
//TODO mkpa/mkpa - ��� ������������ � ������ - ����� ���������� �� ����������� ��������
                      ExecWait "$TEMP\psexec \\$IP_Srv2_State /accepteula -u $Srv2_Usr_State -p $Srv2_Pswd_State -c -h -f server2.exe cmd server2.exe /S"
                      IfErrors 0 +2
                               MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� ������� �������. $\n���������� ���������� ������ ������� �� ������ � �������:$\n   $IP_Srv2_State"
                      ClearErrors
                      ExecWait '$TEMP\psexec \\$IP_Srv2_State /accepteula -u $Srv2_Usr_State -p $Srv2_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v ps_addr /d 192.168.1.205 /f'
                      ExecWait '$TEMP\psexec \\$IP_Srv2_State /accepteula -u $Srv2_Usr_State -p $Srv2_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v ps_port /d 10001 /f'

                         
              ${Else}
                     ;SetOutPath "$TEMP"
 	                  FILE "server2.exe"
                      ExecWait "server2.exe"
              ${EndIf}
        ${EndIf}
 SectionEnd
;--------------------------------------------------------------------------
 Section "������� ���" Monitor
 SectionIn 2 4 5
        SetAutoClose false
        SetRebootFlag true
        ${If} $CheckBoxMon_State != ${BST_CHECKED}
               SetOutPath "$TEMP"
               FILE "${PROJECT_ROOT}\Vendor\PsExec.exe"
               FILE "monitor.exe"
               ClearErrors
               ExecWait "$TEMP\psexec \\$IP_Mon_State /accepteula -u $Mon_Usr_State -p $Mon_Pswd_State -c -h -f monitor.exe cmd monitor.exe /S"
               IfErrors 0 +2
                   MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� ��������. $\n���������� ���������� ������� ������� �� ������ � �������:$\n   $IP_Mon_State"
        ${Else}
               SetOutPath "$TEMP"
 	       FILE "monitor.exe"
 	       ExecWait "monitor.exe"
	    ${EndIf}
 SectionEnd

 ;--------------------------------------------------------------------------
 Section "���2 ���-�" ASN
 SectionIn 2 4 5
        SetAutoClose false
        SetRebootFlag true
        ${If} $CheckBoxASN_State != ${BST_CHECKED}
               SetOutPath "$TEMP"
               StrCpy $ServiceName "ASN_Serv"
               Call FindAndDestroy
               FILE "${PROJECT_ROOT}\Vendor\PsExec.exe"
               FILE "ASN.exe"
               ClearErrors
               ExecWait "$TEMP\psexec \\$IP_ASN_State /accepteula -u $ASN_Usr_State -p $ASN_Pswd_State -c -h -f ASN.exe cmd ASN.exe /S"
               IfErrors 0 +2
                   MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� ������� ��� ���-�. $\n���������� ���������� ������ ��� ������� �� ������ � �������:$\n   $IP_ASN_State"
        ${Else}
               SetOutPath "$TEMP"
 	       FILE "ASN.exe"
 	       ExecWait "ASN.exe"
	${EndIf}
 SectionEnd

  ;--------------------------------------------------------------------------
 Section "�� ���07 + ���3" MBK07
 SectionIn 2 4 5

        SetAutoClose false
        SetRebootFlag true
        ${If} $CheckBoxOff07_State != ${BST_CHECKED}
              ${If} $CheckBoxMBK07_State != ${BST_CHECKED}
                     SetOutPath "$TEMP"
                     StrCpy $ServiceName "MBK07"
                     Call FindAndDestroy
                     FILE "${PROJECT_ROOT}\Vendor\PsExec.exe"
                     FILE "MBK07.exe"
                     ClearErrors
                     ExecWait "$TEMP\psexec \\$IP_MBK07_State /accepteula -u $MBK07_Usr_State -p $MBK07_Pswd_State -c -h -f MBK07.exe cmd MBK07.exe /S"
                     IfErrors 0 +2
                              MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� ������� �� ���-07. $\n���������� ���������� ������ �� ���07 ������� �� ������ � �������:$\n   $IP_ASN_State"
              ${Else}
                     SetOutPath "$TEMP"
 	                 FILE "MBK07.exe"
 	                 ExecWait "MBK07.exe"
	        ${EndIf}
        ${EndIf}
        
        ${If} $CheckBoxOff3_State != ${BST_CHECKED}
              ${If}   $CheckBoxSrv3_State != ${BST_CHECKED}
                      FILE "server3.exe"

                      ClearErrors
//TODO mkpa/mkpa - ��� ������������ � ������ - ����� ���������� �� ����������� ��������
                      ExecWait "$TEMP\psexec \\$IP_Srv3_State /accepteula -u $Srv3_Usr_State -p $Srv3_Pswd_State -c -h -f server3.exe cmd server3.exe /S"
                      IfErrors 0 +2
                               MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� �������� �������. $\n���������� ���������� ������ ������� �� ������ � �������:$\n   $IP_Srv3_State"
                      ClearErrors
;                      ExecWait '$TEMP\psexec \\$IP_Srv2_State /accepteula -u $Srv2_Usr_State -p $Srv2_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v mec_addr /d 192.168.1.20 /f'
;                      ExecWait '$TEMP\psexec \\$IP_Srv2_State /accepteula -u $Srv2_Usr_State -p $Srv2_Pswd_State -h REG ADD "HKCU\Software\Cometa\��� ���� ���" /v mec_port /d 10000 /f'

              ${Else}
                      SetOutPath "$TEMP"
 	                  FILE "server3.exe"
                      ExecWait "server3.exe"
              ${EndIf}
        ${EndIf}
 SectionEnd
 ;--------------------------------------------------------------------------
 Section "������ ���-��" KPAM4
 SectionIn 2 4 5
        SetAutoClose false
        SetRebootFlag true
        ${If} $CheckBoxM4_State != ${BST_CHECKED}
               SetOutPath "$TEMP"
               FILE "${PROJECT_ROOT}\Vendor\PsExec.exe"
               FILE "kpa_mch.exe"
               ClearErrors
               ExecWait "$TEMP\psexec \\$IP_M4_State /accepteula -u $M4_Usr_State -p $M4_Pswd_State -c -h -f kpa_mch.exe cmd monitor.exe /S"
               IfErrors 0 +2
                   MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ���������� �������� ��������� ������� ���-��. $\n���������� ���������� ��� ������� �� ������ � �������:$\n   $IP_M4_State"
        ${Else}
               SetOutPath "$TEMP"
 	       FILE "kpa_mch.exe"
 	       ExecWait "kpa_mch.exe"
	    ${EndIf}
 SectionEnd
*/
;--------------------------------------------------------------------------
/*
Section "WinMerge" Winmerge
        SetOutPath "$TEMP"
        ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$PROGRAMFILES\WinMerge"
        FILE "..\WinMerge\WinMerge-2.12.4-Setup.exe"
        ExecWait '"$TEMP\WinMerge-2.12.4-Setup.exe" /silent'
        Delete "$TEMP\WinMerge-2.12.4-Setup.exe"
SectionEnd
*/
;--------------------------------
/*
	Section "��������" ;��������
		SectionIn 5
		#    SetOutPath $INSTDIR\logs
		#    FILE /a /r "cmake_release\logs\*.*"
	SectionEnd
*/
;--------------------------------------------------------------------------
/*
 Section "������������" Documentation
 SectionIn 1 2 3 4 5
           RMDir /r "$INSTDIR\������������"
	   SetOutPath "$INSTDIR\������������"
	  # �������� ��� ������������.... ���������� ��� ������ �����
	  # FILE /a /r "..\docs\latex\Maindoc1.pdf"
	  # FILE /a /r "..\docs\latex\Maindoc2.pdf"

	   FILE /a /r "�� ����\docs\*.doc"
	   FILE /a /r "�� ����\docs\*.docx"
 SectionEnd
*/
;--------------------------------------------------------------------------
;Descriptions

  ;Language strings
;==========================================================================
; �������

;===========================================================================
;================== ������ ������ ��� ����������� �� ������� ===============
;===========================================================================

  Function "FindAndDestroy"
    	         ClearErrors
    	         DetailPrint "����� ��������� $ServiceName"
    	         #MessageBox MB_OK|MB_ICONEXCLAMATION "����� ��������� $ServiceName"
                 ;SimpleSC::GetServiceStatus $ServiceName
                 Pop $0 ; ���������� ��� ������ ���� ������� �� ����������: (!=0) � ��������� ������ ����� �����: (0)
                 Pop $1 ; ���������� ������ ������� (�� ����:)
                 DetailPrint "��� ���������:$\n ��� ������: $0 $\n ������ ������: $1"
                 #MessageBox MB_OK|MB_ICONEXCLAMATION "��� ���������:$\n ��� ������: $0 $\n ������ ������: $1"
/*  1 - STOPPED
    2 - START_PENDING
    3 - STOP_PENDING
    4 - RUNNING
    5 - CONTINUE_PENDING
    6 - PAUSE_PENDING
    7 - PAUSED
*/
                 ${If} $0 == 0       ; ���� ������ ����, ����� ��������� � ������
                       ${If} $1 == 4
                             DetailPrint "$ServiceName ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"
                             #MessageBox MB_OK|MB_ICONEXCLAMATION "$ServiceName ������������ �� ���������� � �������!$\n ��� ������: $0 $\n ������ ������: $1"

                             ;SimpleSC::StopService $ServiceName 1 30 ; ����� ����� ����������� ���� � ������� 30 ������.
                             Pop $R0

                             StrCmp $R0 0 dead_meat why_wont_you_die ;��������� ��������� ��� �������� �������� � ������ �� �� �������!?
                             why_wont_you_die:
                                        DetailPrint  "�� ������� ��������� ������� $ServiceName. ��������� ������� �������."
                                        #MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� $ServiceName. ��������� ������� �������."
                             dead_meat:
                                        DetailPrint  "������ $ServiceName ������� �����������."
                       ${EndIf}
                 ${EndIf}

        FunctionEnd

/*
Function "SetupServer1"
        StrCpy $ServiceName "QCOMServer1"
	    Call FindAndDestroy
	    SetOutPath "$INSTDIR\������ 1"
	    FILE /r /x "cbk_pkodll.dll" /x "CBKFlash.dll"  "�� ����\��������\*.dll"
FunctionEnd

Function "SetupServer2"
        StrCpy $ServiceName "QCOMServer2"
	    Call FindAndDestroy
	    SetOutPath "$INSTDIR\������ 2"
	    FILE /r /x "cbk_pkodll.dll" /x "CBKFlash.dll"  "�� ����\��������\*.dll"
FunctionEnd
*/
;===========================================================================
;***************************************************************************
;===========================================================================



	Function "ClientShortCut"

		CreateDirectory "$SMPROGRAMS\������\����\���\������"

                
		CreateShortCut "$SMPROGRAMS\������\����\���\������\������� ������.lnk" "$INSTDIR\������\uninst_client.exe"
		CreateShortCut "$DESKTOP\�������� ������.lnk" "C:\WINDOWS\system32\runas.exe" '/user:%COMPUTERNAME%\$Srv1_Usr_State "$INSTDIR\������\Client.exe"' "$INSTDIR\������\Client.exe"
		CreateShortCut "$DESKTOP\��������� ������.lnk" "$INSTDIR\������\Client.exe" "--noremote"
		CreateShortCut "$SMPROGRAMS\������\����\���\������\������.lnk" '"$INSTDIR\������\Client.exe" --noremote'
		CreateShortCut "$SMPROGRAMS\������\����\���\������\�������� ������.lnk" "C:\WINDOWS\system32\runas.exe" '/savecred /user:%COMPUTERNAME%\$Srv1_Usr_State "$INSTDIR\������\Client.exe"'

                SetOutPath "$DESKTOP"
              #  FILE  /r /x "restart.bat" "${TARGET}\�������\*.bat"
#                FILE "�� ����\psExec.exe"
	FunctionEnd


	Function "DirectoryPre"

                 SectionGetFlags ${Client} $0
                 IntOp $0 $0 & 1
                 StrCmp $0 0 0 +2
                 Abort
	
	FunctionEnd

;                SetRebootFlag true


;--------------------------------------------------------------------------
;�������, ���������� ����� ����� ������� ���������

  Function .onInit
           ;��������� ������ �� �������� ����������� �� �������, ���� ����

           ReadRegStr $IP_Srv1_State HKLM "Software\Cometa\��� ���� ���" "IP_srv1_only"
           ${If} $IP_Srv1_State == ""
                 StrCpy $IP_Srv1_State "192.168.1.101"
           ${EndIf}

           ReadRegStr $IP_Srv2_State HKLM "Software\Cometa\��� ���� ���" "IP_srv2_only"
           ${If} $IP_Srv2_State == ""
                 StrCpy $IP_Srv2_State "192.168.1.201"
           ${EndIf}

           ReadRegStr $IP_Mon_State HKLM "Software\Cometa\��� ���� ���" "IP_mon_only"
           ${If} $IP_Mon_State == ""
                 StrCpy $IP_Mon_State "192.168.1.11"
           ${EndIf}
           
           ReadRegStr $IP_ASN_State HKLM "Software\Cometa\��� ���� ���" "IP_ASN_only"
           ${If} $IP_ASN_State == ""
                 StrCpy $IP_ASN_State "192.168.1.14"
           ${EndIf}

           ReadRegStr $IP_MBK07_State HKLM "Software\Cometa\��� ���� ���" "IP_MBK07_only"
           ${If} $IP_MBK07_State == ""
                 StrCpy $IP_MBK07_State "192.168.1.13"
           ${EndIf}
           
           ReadRegStr $IP_Srv3_State HKLM "Software\Cometa\��� ���� ���" "IP_srv3_only"
           ${If} $IP_Srv3_State == ""
                 StrCpy $IP_Srv3_State "192.168.1.13"
           ${EndIf}
           
           ReadRegStr $IP_M4_State HKLM "Software\Cometa\��� ���� ���" "IP_M4_only"
           ${If} $IP_M4_State == ""
                 StrCpy $IP_M4_State "192.168.1.13"
           ${EndIf}

           ReadRegStr $Srv1_Usr_State HKLM "Software\Cometa\��� ���� ���" "Srv1_usr"
           ${If} $Srv1_Usr_State == ""
                 StrCpy $Srv1_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv1_Pswd_State HKLM "Software\Cometa\��� ���� ���" "Srv1_pswd"
           ${If} $Srv1_Pswd_State == ""
                 StrCpy $Srv1_Pswd_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv2_Usr_State HKLM "Software\Cometa\��� ���� ���" "Srv2_usr"
           ${If} $Srv2_Usr_State == ""
                 StrCpy $Srv2_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv2_Pswd_State HKLM "Software\Cometa\��� ���� ���" "Srv2_pswd"
           ${If} $Srv2_Pswd_State == ""
                 StrCpy $Srv2_Pswd_State "mkpa"
           ${EndIf}

           ReadRegStr $Mon_Usr_State HKLM "Software\Cometa\��� ���� ���" "Mon_usr"
           ${If} $Mon_Usr_State == ""
                 StrCpy $Mon_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Mon_Pswd_State HKLM "Software\Cometa\��� ���� ���" "Mon_pswd"
           ${If} $Mon_Pswd_State == ""
                 StrCpy $Mon_Pswd_State "mkpa"
           ${EndIf}
           
           ReadRegStr $ASN_Usr_State HKLM "Software\Cometa\��� ���� ���" "ASN_usr"
           ${If} $ASN_Usr_State == ""
                 StrCpy $ASN_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $ASN_Pswd_State HKLM "Software\Cometa\��� ���� ���" "ASN_pswd"
           ${If} $ASN_Pswd_State == ""
                 StrCpy $ASN_Pswd_State "mkpa"
           ${EndIf}

           ReadRegStr $MBK07_Usr_State HKLM "Software\Cometa\��� ���� ���" "MBK07_usr"
           ${If} $MBK07_Usr_State == ""
                 StrCpy $MBK07_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $MBK07_Pswd_State HKLM "Software\Cometa\��� ���� ���" "MBK07_pswd"
           ${If} $MBK07_Pswd_State == ""
                 StrCpy $MBK07_Pswd_State "mkpa"
           ${EndIf}
           
           ReadRegStr $Srv3_Usr_State HKLM "Software\Cometa\��� ���� ���" "Srv3_usr"
           ${If} $Srv3_Usr_State == ""
                 StrCpy $Srv3_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Srv3_Pswd_State HKLM "Software\Cometa\��� ���� ���" "Srv3_pswd"
           ${If} $Srv3_Pswd_State == ""
                 StrCpy $Srv3_Pswd_State "mkpa"
           ${EndIf}
           
           ReadRegStr $M4_Usr_State HKLM "Software\Cometa\��� ���� ���" "M4_usr"
           ${If} $M4_Usr_State == ""
                 StrCpy $M4_Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $M4_Pswd_State HKLM "Software\Cometa\��� ���� ���" "M4_pswd"
           ${If} $M4_Pswd_State == ""
                 StrCpy $M4_Pswd_State "mkpa"
           ${EndIf}
  FunctionEnd
;==========================================================================
;******************* ���������� ����� ����� �������!!! ********************
;==========================================================================
!include Archive.nsh
/*
!include Servers.nsh
!include Monitor.nsh
!include kpa_ASN.nsh
!include kpa_mbk07.nsh
!include kpa_mch.nsh
*/
;==========================================================================
;**************************************************************************
;==========================================================================
  LangString DESC_Client ${LANG_RUSSIAN} "���������� ����������."
  LangString DESC_Servers ${LANG_RUSSIAN} "��������� ����������."
  LangString DESC_Monitor ${LANG_RUSSIAN} "������ ���."
  LangString DESC_ASN ${LANG_RUSSIAN} "���2 ���-�."
  LangString DESC_MBK07 ${LANG_RUSSIAN} "�� ���07 + ���3"
  LangString DESC_KPAM4 ${LANG_RUSSIAN} "���-��"
 # LangString DESC_Winmerge ${LANG_RUSSIAN} "���������� ��� ����������� ��������� �������."
  LangString DESC_Documentation ${LANG_RUSSIAN} "������������."
  
;--------------------------------------------------------------------------
;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  
    !insertmacro MUI_DESCRIPTION_TEXT ${Client} $(DESC_Client)
    !insertmacro MUI_DESCRIPTION_TEXT ${Servers} $(DESC_Servers)
    !insertmacro MUI_DESCRIPTION_TEXT ${Monitor} $(DESC_Monitor)
    !insertmacro MUI_DESCRIPTION_TEXT ${ASN} $(DESC_ASN)
    !insertmacro MUI_DESCRIPTION_TEXT ${MBK07} $(DESC_MBK07)
    !insertmacro MUI_DESCRIPTION_TEXT ${KPAM4} $(DESC_KPAM4)
#   !insertmacro MUI_DESCRIPTION_TEXT ${Winmerge} $(DESC_Winmerge)
    !insertmacro MUI_DESCRIPTION_TEXT ${Documentation} $(DESC_Documentation)
    
  !insertmacro MUI_FUNCTION_DESCRIPTION_END