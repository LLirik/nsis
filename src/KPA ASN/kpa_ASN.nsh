!IFNDEF KPA_ASN_NSH
!define kpa_asn_nsh

;==========================================================================
; ����������� ��������������� �������

  !include "MUI2.nsh"                ;����� �������� ���������
  !include LogicLib.nsh              ;����������� �������������
  !include nsDialogs.nsh             ;�������� ����������� ����
  !include "Plugins\include\ValidateIPAdress.nsh"      ;�������� ������������ ����� IP-������

;==========================================================================
;���� �������
;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� ����� ���

  Function EnDisableASNBox
	Pop $CheckBoxASN
	${NSD_GetState} $CheckBoxASN $0
	${If} $0 == 0
		EnableWindow $IP_ASN 1
		EnableWindow $ASN_Usr 1
		EnableWindow $ASN_Pswd 1
		call ASNIPAddrTextChange
	${Else}
		EnableWindow $IP_ASN 0
		EnableWindow $ASN_Usr 0
		EnableWindow $ASN_Pswd 0
		call ASNIPAddrTextChange
	${EndIf}
  FunctionEnd

;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������ 
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function ASNIPAddrTextChange

        ClearErrors
        Pop $1 # $1 == $IP_ASN

	${NSD_GetText} $IP_ASN $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $ASN_Err "1"                 #
        ${Else}
              StrCpy $ASN_Err "0"
        ${EndIf}                                  #

        Call EnDisableASNNextButton

  FunctionEnd
;--------------------------------------------------------------------------
;�������, ������������� ������ "�����" ��� ��������� �������� IP ������

  Function EnDisableASNNextButton

        GetDlgItem $ASNNextButton $HWNDPARENT 1

        Pop $CheckBoxASN
	${NSD_GetState} $CheckBoxASN $1

        IntOp $1 $1 !
        IntOp $Case4 $ASN_Err && $1

        ${If} $Case4  == 1
              EnableWindow $ASNNextButton 0
        ${Else}
              EnableWindow $ASNNextButton 1
              ;MessageBox MB_OK "�� ������"
        ${EndIf}
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ��������� ��������

  Function ASNPageCreate

        SectionGetFlags ${ASN} $R0
        Pop $R0
        ${If} $R0 == 1
              goto show
        ${EndIf}
        abort
        show:

	nsDialogs::Create 1018        ;������� ��������, � "���������" � �����
	Pop $ASNDialog                   ;����������� ��� � ����������

	${If} $ASNDialog == error
              MessageBox MB_OK "�������� �������� ��� ���������. ���������� � ������������. $\n $ASNDialog"
	      Abort
	${EndIf}

	${NSD_CreateLabel} 15% 0% 100% 18% "����� ������������ ��������� �������� ���������. $\n\
                                         �������� �������� ���������: �������� ��� ���������.$\n\
                                         ��� ������������� ������� IP-����� ��������(��) �����(�)."
	Pop $ASNLabel
;=================================================================================================================
	${NSD_CreateCheckBox} 10%% 27% 92% 13% "�������� ��� ��������� ���������� ������� ���2 ���-�."
        Pop $CheckBoxASN
	    ${NSD_OnClick} $CheckBoxASN EnDisableASNBox ;TODO ������� ����� ����� �������� EnDisableBox
/*******************************************     GROUP BOXES    **************************************************/
        ${NSD_CreateGroupBox} 0% 19% 100% 70% "�������� ��������� �� ���2 ���-�"
        Pop $ASNGBox
/*******************************************     IP ��������   ****************************************************/
	${NSD_CreateLabel} 10% 45% 13% 9% "IP-�����:"
	Pop $ASNLabel

	${NSD_CreateText} 27% 44% 60% 9% $IP_ASN_State
        Pop $IP_ASN
/*******************************************  ����� �� ��������  **************************************************/
	${NSD_CreateLabel} 10% 57% 13% 9% "�����:"
	Pop $ASNLabel

        ${NSD_CreateText} 27% 55% 60% 9% $ASN_Usr_State
        Pop $ASN_Usr
/*******************************************  ������ �� ��������  **************************************************/
        ${NSD_CreateLabel} 10% 67% 13% 9% "������:"
	Pop $ASNLabel

        ${NSD_CreateText} 27% 66% 60% 9% $ASN_Pswd_State
        Pop $ASN_Pswd
;=====================================================================================================================

        ; �� ��������� IP-������ ���������
        EnableWindow $IP_ASN 1
        EnableWindow $ASN_Usr 1
        EnableWindow $ASN_Pswd 1

        ${NSD_OnChange} $IP_ASN ASNIPAddrTextChange

        ${If} $CheckBoxASN_State == ${BST_CHECKED}
		${NSD_Check} $CheckBoxASN
                EnableWindow $IP_ASN 0
                EnableWindow $ASN_Usr 0
                EnableWindow $ASN_Pswd 0
	${EndIf}

	call ASNIPAddrTextChange

	nsDialogs::Show

  FunctionEnd
;--------------------------------------------------------------------------
;�������, �������������� ��� ����� �� ��������
  Function ASNPageLeave

        ${NSD_GetState} $CheckBoxASN $CheckBoxASN_State
        ${If} $CheckBoxASN_State != ${BST_CHECKED}

        ${Else}
                StrCpy $IP_ASN_State "192.168.1.13"
                ${NSD_SetText} $IP_ASN $IP_ASN_State
	${EndIf}


        ${NSD_GetText} $IP_ASN $IP_ASN_State

        ${NSD_GetText} $ASN_Usr $ASN_Usr_State
        ${NSD_GetText} $ASN_Pswd $ASN_Pswd_State

        ${NSD_GetState} $CheckBoxASN $CheckBoxASN_State

        ;MessageBox MB_OK "(V)(^,,,^)(V)$\n $\n   \(0o0)/ $\n $Srv_usr_State $\n $Srv_pswd_State $\n $ASN_usr_State $\n $ASN_pswd_State"

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_ASN" $IP_ASN_State

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "ASN_usr" $ASN_Usr_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "ASN_pswd" $ASN_Pswd_State

        StrCpy $ASN_Err "0"
        DetailPrint "����� ��� ���: $IP_ASN_State"
        ;����-��������
        ;MessageBox MB_OK "V(^__^)V $\n����� �������: $IP_srv_State$\n$\n����� ��������: $IP_ASN_State$\n$\n"
  FunctionEnd
;==========================================================================
;MessageBox MB_OK "V(^__^)V" - ����-��������
;MessageBox MB_OK "(V)(^,,,^)(V)" - �����-������-��������
;MessageBox MB_OK "\(0o0)/ - ���� ��������"

;MessageBox MB_OK  (\.../) (\.../) (\.../) (\.../) $\n ( *.*) ('.'= ) ('.'= ) ('.'= ) $\n (")_(") (")_(") (")_(") (")_(") $\n ��������� ��������-����������"
!endif