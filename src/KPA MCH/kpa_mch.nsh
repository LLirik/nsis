!ifndef KPAM4_NSH
!define KPAM4_NSH

;==========================================================================
; ����������� ��������������� �������

  !include "MUI2.nsh"                  ;����� �������� ���������
  !include LogicLib.nsh              ;����������� �������������
  !include nsDialogs.nsh             ;�������� ����������� ����
  !include Plugins\include\ValidateIPAdress.nsh      ;�������� ������������ ����� IP-������
  

;==========================================================================
;���� �������


/*
  TODO ����������� ���������� ��� ������ ����� �������.
       ��� �����, ������ ����������� �������� � ���� ��� � "���������"
       �� TextBox
  end TODO
*/
;--------------------------------------------------------------------------
;�������, ����������� � �������������� ���� � IP ������� ��������

  Function EnDisableM4Box
	Pop $CheckBoxM4
	${NSD_GetState} $CheckBoxM4 $0
	${If} $0 == 0
		EnableWindow $IP_M4 1
		EnableWindow $M4_Usr 1
		EnableWindow $M4_Pswd 1
		call M4IPAddrTextChange
	${Else}
		EnableWindow $IP_M4 0
		EnableWindow $M4_Usr 0
		EnableWindow $M4_Pswd 0
		call M4IPAddrTextChange
	${EndIf}
  FunctionEnd
  
;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������ �������
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function M4IPAddrTextChange

        ClearErrors
        Pop $1 # $1 == $IP_mon
        
	${NSD_GetText} $IP_M4 $0                 #
        Push $0                                   #
        Call ValidateIP                           #
                                                  #
        ${If} ${Errors}                           #
              StrCpy $M4_Err "1"                 #
        ${Else}
              StrCpy $M4_Err "0"
        ${EndIf}                                  #

        Call EnDisableM4NextButton
        
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ������������� ������ "�����" ��� ��������� �������� IP ������

  Function EnDisableM4NextButton

        GetDlgItem $M4NextButton $HWNDPARENT 1

        Pop $CheckBoxM4
	    ${NSD_GetState} $CheckBoxM4 $1

        IntOp $1 $1 !
        IntOp $Case7 $M4_Err && $1

        ${If} $Case7  == 1
              EnableWindow $M4NextButton 0
        ${Else}
              EnableWindow $M4NextButton 1
              ;MessageBox MB_OK "�� ������"
        ${EndIf}
  FunctionEnd
;--------------------------------------------------------------------------
;�������, ��������� ��������

  Function KPAM4PageCreate
  
        SectionGetFlags ${KPAM4} $R0
        Pop $R0
        ${If} $R0 == 1
              goto show
        ${EndIf}
        abort
        show:
  
	nsDialogs::Create 1018        ;������� ��������, � "���������" � �����
	Pop $M4Dialog                   ;����������� ��� � ����������

	${If} $M4Dialog == error
              MessageBox MB_OK "�������� �������� ��� ���������. ���������� � ������������. $\n $M4Dialog"
	      Abort
	${EndIf}

	${NSD_CreateLabel} 15% 0% 100% 18% "����� ������������ ��������� �������� ���������. $\n\
                                         �������� �������� ���������: �������� ��� ���������.$\n\
                                         ��� ������������� ������� IP-����� ��������(��) �����(�)."
	Pop $M4Label
;=================================================================================================================
	${NSD_CreateCheckBox} 10%% 27% 92% 13% "�������� ��� ��������� ���������� ������� ���-��."
        Pop $CheckBoxM4
	${NSD_OnClick} $CheckBoxM4 EnDisableM4Box ;TODO ������� ����� ����� �������� EnDisableBox
/*******************************************     GROUP BOXES    **************************************************/
        ${NSD_CreateGroupBox} 0% 19% 100% 70% "�������� ��������� ������� ���-��"
        Pop $M4GBox
/*******************************************     IP ��������   ****************************************************/
	${NSD_CreateLabel} 10% 45% 13% 9% "IP-�����:"
	Pop $M4Label

	${NSD_CreateText} 27% 44% 60% 9% $IP_M4_State
        Pop $IP_M4
/*******************************************  ����� �� ��������  **************************************************/
	${NSD_CreateLabel} 10% 57% 13% 9% "�����:"
	Pop $M4Label

        ${NSD_CreateText} 27% 55% 60% 9% $M4_Usr_State
        Pop $M4_Usr
/*******************************************  ������ �� ��������  **************************************************/
        ${NSD_CreateLabel} 10% 67% 13% 9% "������:"
	Pop $M4Label

        ${NSD_CreateText} 27% 66% 60% 9% $M4_Pswd_State
        Pop $M4_Pswd
;=====================================================================================================================

        ; �� ��������� IP-������ ���������
        EnableWindow $IP_M4 1
        EnableWindow $M4_Usr 1
        EnableWindow $M4_Pswd 1

        ${NSD_OnChange} $IP_M4 M4IPAddrTextChange
        
        ${If} $CheckBoxM4_State == ${BST_CHECKED}
		${NSD_Check} $CheckBoxM4
                EnableWindow $IP_M4 0
                EnableWindow $M4_Usr 0
                EnableWindow $M4_Pswd 0
	${EndIf}
	
	call M4IPAddrTextChange

	nsDialogs::Show

  FunctionEnd
;--------------------------------------------------------------------------
;�������, �������������� ��� ����� �� ��������
  Function KPAM4PageLeave
  
        ${NSD_GetState} $CheckBoxM4 $CheckBoxM4_State
        ${If} $CheckBoxM4_State != ${BST_CHECKED}

        ${Else}
                StrCpy $IP_M4_State "192.168.1.13"
                ${NSD_SetText} $IP_M4 $IP_M4_State
	${EndIf}

	
        ${NSD_GetText} $IP_M4 $IP_M4_State
        
        ${NSD_GetText} $M4_Usr $M4_Usr_State
        ${NSD_GetText} $M4_Pswd $M4_Pswd_State
        
        ${NSD_GetState} $CheckBoxM4 $CheckBoxM4_State

        ;MessageBox MB_OK "(V)(^,,,^)(V)$\n $\n   \(0o0)/ $\n $Srv_usr_State $\n $Srv_pswd_State $\n $Mon_usr_State $\n $Mon_pswd_State"

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "IP_M4" $IP_M4_State

        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "M4_usr" $M4_Usr_State
        WriteRegStr HKLM "Software\Cometa\��� ���� ���" "M4_pswd" $M4_Pswd_State
        
        StrCpy $M4_Err "0"
        DetailPrint "����� ��������: $IP_M4_State"
        ;����-�������� 
        ;MessageBox MB_OK "V(^__^)V $\n����� �������: $IP_srv_State$\n$\n����� ��������: $IP_mon_State$\n$\n"
  FunctionEnd
;==========================================================================
;MessageBox MB_OK "V(^__^)V" - ����-��������
;MessageBox MB_OK "(V)(^,,,^)(V)" - �����-������-��������
;MessageBox MB_OK "\(0o0)/ - ���� ��������"

;MessageBox MB_OK  (\.../) (\.../) (\.../) (\.../) $\n ( *.*) ('.'= ) ('.'= ) ('.'= ) $\n (")_(") (")_(") (")_(") (")_(") $\n ��������� ��������-����������"
!endif