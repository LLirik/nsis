	!include LogicLib.nsh
	!include FileFunc.nsh

  
	!insertmacro Locate
  
	!define PROJECT_ROOT "..\.."
	!ifndef TARGET
		!define TARGET "target_template"
	!endif
	!define TARGET_ROOT "${PROJECT_ROOT}\${TARGET}\�������� ������"
	!define SERVICE_NAME "QCOMFrameServer"
	!define PROJECT_NAME "��� ���� ���"
	!define INSTDIR "�������� ������"
	!define COMAPP_NAME "ComappFrame"
	!define COMAPP_GUID "{0495F7F0-DDA6-4A1D-B9E0-C38943FC7D73}"
	/*WTF*/
	!define SECTIONCOUNT 5
	InstallDir "$PROGRAMFILES64\Cometa\${PROJECT_NAME}\${INSTDIR}"
	
	!addplugindir "Plugins\bin"
	!addplugindir "${PROJECT_ROOT}\plugins\bin"

	;--------------------------------
	Name "${PROJECT_NAME}"
	OutFile "${PROJECT_ROOT}\FrameServer.exe"
	;--------------------------------
	SetDatablockOptimize on
	XPStyle on
	AutoCloseWindow false
	ShowInstDetails show
	;SetCompressor lzma
	
	BrandingText "��� $\"���������� ������$\" ����-44$\n2011-2015"
	LoadLanguageFile "${NSISDIR}\Contrib\Language files\Russian.nlf"
	;--------------------------------

	;--------------------------------
	RequestExecutionLevel admin
	;--------------------------------
	; Pages

	Page components
	Page directory
	Page instfiles

	UninstPage uninstConfirm
	UninstPage instfiles    

	;--------------------------------
	;!include EnvVarUpdate.nsh
	;--------------------------------
	Var /GLOBAL switch_overwrite
	!include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"	
	;--------------------------------
	;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	;  ��������� ������ ������ ����
	!include "..\mainServerSrc.nsh"	
	;vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	;--------------------------------
	; 
	Section ""
		Push 0
		Call FindAndDestroyService
		RMDir /r "$INSTDIR_backup"
		StrCpy $switch_overwrite 1
		!insertmacro MoveFolder "$INSTDIR" "$INSTDIR_backup" "*.*"
	SectionEnd
	
	SectionGroup "��������"
		Section "�������� VISA"	visa
			;TODO �������� ������ MSI ������ ��� VISA, UNMBASE, UNMBKUPI, UNMKPRM	SectionGroupEnd
			SetOutPath $TEMP
			FILE /r "${PROJECT_ROOT}\vendor\VISA"
			ExecWait  VISA\InftestVISA\Setup.exe
			IfFileExists "$SYSDIR\visa32.dll" +1
			CopyFiles "$PROGRAMFILES\Informtest\VISA\SETUP\visa32.dll" "$SYSDIR"

			ExecWait "VISA\cvirte\setup.exe /q /acceptlicenses yes /r:n"
		
			ExecWait "msiexec /package VISA\UNMBASE\unmbase.msi /passive"
			ExecWait "msiexec /package VISA\UNMKPRM\unmkprm.msi /passive"
			ExecWait "msiexec /package VISA\UNMBKUPI\unmbkupi.msi /passive"
		SectionEnd

		Section /o "�������� ��������� (������ ��� �������!)" stubs
			SetOutPath "$INSTDIR"
			FILE "${TARGET_ROOT}\..\��������\unmbase_32.dll"
			FILE "${TARGET_ROOT}\..\��������\unmbkupi_32.dll"
			FILE "${TARGET_ROOT}\..\��������\unmkprm_32.dll"

			FILE "${TARGET_ROOT}\..\��������\visa32.dll"
		SectionEnd

  		Section /o "�� ������� ��������" none
  	  
  		SectionEnd
	SectionGroupEnd
	
	Section "������ ������" frame_srv_section
		Push 1 #������� ������
		Call FindAndDestroyService
		SetOutPath "$TEMP"
		FILE "${PROJECT_ROOT}\Vendor\vcredist_x86.exe"
		ExecWait "$TEMP\vcredist_x86.exe /q"
		DELETE "$TEMP\vcredist_x86.exe"

		SetOutPath "$INSTDIR"
		File /r "${TARGET_ROOT}\*.*"

		File "${PROJECT_ROOT}\img\server_frame.ico"

		FILE "${PROJECT_ROOT}\Vendor\regtlibv12.exe"


		Call SetServerPermissions		
		Call ServerShortCut
		
		ClearErrors
		SimpleSC::StartService ${SERVICE_NAME} "" 30 ;������ ������� ��� ���������� � ��������� ������ � 30 ������
		Pop $0
		${If} $0 != 0
			MessageBox MB_OK|MB_ICONEXCLAMATION "�� ������� ��������� ������� ${SERVICE_NAME}.$\n ������=$0 �������� �� ������� ���������$\n��������� ������ �������."
		${EndIf}
		
		WriteUninstaller "uninstall.exe"
	SectionEnd
	
	Section ""
		#TODO ���� ����� ������� ���������� � ����������� �������, ���� ��� ���� ������� (��������� ����� �� ����... ����)
		WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Policies\System" "LocalAccountTokenFilterPolicy" 0x1
	SectionEnd

  Function backup_funck
    SectionGetFlags ${frame_srv_section} $0
    Push 0
    Call FindAndDestroyService
    
    ${IF} $0 == ${SF_SELECTED}  		
    	RMDir /r "$INSTDIR_backup"
    	StrCpy $switch_overwrite 1 
    	!insertmacro MoveFolder "$INSTDIR" "$INSTDIR_backup" "*.*"
    ${EndIf}  
  FunctionEnd

Function .onInit

  StrCpy $1 ${visa} ; Group 1 - Option 1 is selected by default

FunctionEnd
  
  Function .onSelChange
  
    !insertmacro StartRadioButtons $1
      !insertmacro RadioButton ${visa}
      !insertmacro RadioButton ${stubs}
      !insertmacro RadioButton ${none}
    !insertmacro EndRadioButtons
  	
  FunctionEnd

	; Uninstaller

	Var ProgmanHwnd
	Var ShellHwnd
	Var DesktopHwnd

	Function un.RefreshDesktop
	; 256=WM_KEYDOWN
	; 257=WM_KEYUP
	; 116=VK_F5
		FindWindow $ProgmanHwnd "Progman" "Program Manager"
		FindWindow $ShellHwnd "SHELLDLL_DefView" "" $ProgmanHwnd 0
		FindWindow $DesktopHwnd "SysListView32" "" $ShellHwnd 0
		System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 256, i 116, i 0)"
		System::Call "user32.dll::PostMessage(i $DesktopHwnd, i 257, i 116, i 0x80000000)"
	FunctionEnd

	UninstallText "�������� ��������� �������"
	UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\orange-uninstall.ico"

	Section "Uninstall"

		DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PROJECT_NAME}"

		call un.FindAndDestroyService
		
		Delete "$INSTDIR\*.*"
		Delete "$INSTDIR\uninstall.exe"
		Delete /REBOOTOK "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}\*.*" 
		RMDir /r "$SMPROGRAMS\${PROJECT_NAME}\${INSTDIR}"
		
		Delete "$DESKTOP\${INSTDIR} ���������.lnk"
		
		call un.RefreshDesktop
		RMDir /r "$INSTDIR"
		
		IfFileExists "$INSTDIR\*.*" 0 UnNoErrorMsg
			MessageBox MB_OK "��������: ����� $INSTDIR �� ������� ���������!!!" IDOK 0

		UnNoErrorMsg:
	SectionEnd
