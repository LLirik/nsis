  !include "MUI2.nsh"
  !include LogicLib.nsh
  !include nsDialogs.nsh
  !include FileFunc.nsh
  !insertmacro Locate
  
  !define PROJECT_ROOT "..\.."
  
  !include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"
  !include "${PROJECT_ROOT}\Plugins\include\MUI_EXTRAPAGES.nsh"
  
  ;!addplugindir "Plugins\bin"
;--------------------------------------------------------------------------
;General

  Name "��� ���� ��� - �������� ���������� ������������"
  OutFile "${PROJECT_ROOT}\Install_SPO_MKPA_MCA.exe"

  RequestExecutionLevel admin

;==========================================================================
;���������� ����������
 
  Var Dialog          ;���� ���� �������
  Var Label           ;���� ������� "������� IP-������...
  Var RadioButton     ;����� ���� �����
  Var NextButton      ;������ "�����"
  Var GBox            ;GroupBox
  
  Var IP              ;���������� ��� �������� ������� � IP �������
  Var Usr             ;���������� ��� �������� ������� � �������
  Var Pswd            ;���������� ��� �������� ������� � �������

  Var IP_State        ;��������� (���������� �����) IP ������
  Var Usr_State       ;��������� (���������� �����) ������ (�� ��������� "mkpa")
  Var Pswd_State      ;��������� (���������� �����) ������ (�� ��������� "mkpa")

  Var IP_RegName      ;�������� ���������� ��� IP ������, ������� ����� ������������ � ������ (�������� IP_Client)
  Var Usr_RegName     ;�������� ���������� ��� ������, ������� ����� ������������ � ������ (�������� Srv1_usr)
  Var Pswd_RegName    ;�������� ���������� ��� ������, ������� ����� ������������ � ������ (�������� ASN_pswd)

;�������� ��������� (������) ��� edit-box(��)
  Var IP_Client_State ;
  Var IP_Srv1_State   ;����������� ��������� ������ Srv1
  Var IP_Srv2_State   ;����������� ��������� ������ Srv2
  Var IP_Srv3_State   ;������ 3
  Var IP_Mon_State    ;����������� ��������� ������ ��������
  Var IP_ASN_State    ;����������� ��������� ������ ���
  Var IP_MBK07_State  ;����������� ��������� ������ ���07
  Var IP_M4_State     ;����������� ��������� ������ ���-��

  #Var Err             #�����������, �.�. ������������ ������ 1 ��� ��������� �����
  
  Var new             ;���� ����, ��� ������ ����� � �� ��� ������ �� ���� (���� ���������� ����)
;--------------------------------------------------------------------------

;==========================================================================
;��������� ����������

  !define MUI_ABORTWARNING      ;�������� �������������� ��������� ��� ������ ���������
  !define MUI_ABORTWARNING_TEXT "�� ������ �������� ��������� �������?"

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "${PROJECT_ROOT}\img\orange.bmp"
  !define MUI_WELCOMEFINISH_BITMAP "${PROJECT_ROOT}\img\orange.bmp"
  !define MUI_ICON "${PROJECT_ROOT}\img\gear.ico"

  !define MUI_FINISHPAGE_NOAUTOCLOSE ;�� ��������� �������� � �������� ���������
  
;===========================================================================
;��������

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_README "${PROJECT_ROOT}\Changelog.txt"
 ; !insertmacro MUI_PAGE_COMPONENTS
  Page custom PageCreate PageLeave

  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

;===========================================================================
;����
  ;Add 1st language
  !insertmacro MUI_LANGUAGE "English"


  ;Add 2nd language
  !insertmacro MUI_LANGUAGE "Russian"

  ;set up install lang strings for second lang
  ${ReadmeLanguage} "${LANG_RUSSIAN}" \
          "������ ���������" \
          "���������� ���������� ��������� ���� ����������." \
          "������ � ������������ � ��� ���������:" \
          "$\n  ��� ��������� ����������� ������� ��������� ��� ������� PageDown."

;===========================================================================
;Installer Sections
;---------------------------------------------------------------------------
 Section ""
         SetRebootFlag true

         SetOutPath "$TEMP"
         ;FILE "Setup.exe"

         ReadRegStr $new HKLM "Software\Cometa\��� ���� ���" "Crab"
         ${If} $new == "(V)(^__^)(V)"
               DetailPrint "������ ���������, � ������� ���� ����"
               DetailPrint "(V)(^__^)(V)"
               ExecWait "$TEMP\Setup.exe"
               SetRebootFlag false
         ${Else}
               WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\RunOnce" "Setup_MKPA" "$TEMP\Setup.exe"
#-----------------------------------------------------------------------------------
# ���������� ����� ���� ������� �� ���������. ������� ������ ����
               call SetIPAdress
               call SwitchFirewallOff
               call SuchAMessage
               call AddNewUser
               call SecurityPolicies
               call DCOM_Settings
#-----------------------------------------------------------------------------------               
               WriteRegStr HKLM "Software\Cometa\��� ���� ���" "Crab" "(V)(^__^)(V)"
         ${EndIf}

 SectionEnd
;===========================================================================
;���� �������
;===========================================================================

;--------------------------------------------------------------------------
;������� ��������� IP ������ ��� ����� �����

	Function "SetIPAdress"
	    ClearErrors
      ExecWait 'netsh interface ipv4 set address "����������� �� ��������� ����" static $IP_State 255.255.255.0'
      IfErrors 0 +2
        MessageBox MB_OK `������ �������� ����������� �� ��������� ����. ���������� $\n IP �����: $IP_State $\n ����� ����: 255.255.255.0$\n � ���������� ���������, ����� ��`
      ClearErrors
      ExecWait 'netsh interface ipv4 set address "����������� �� ��������� ���� 2" static $IP_State 255.255.255.0'
;     IfErrors 0 +2
;       MessageBox MB_OK `������ �������� ����������� �� ��������� ���� 2. ���������� $\n IP �����: $IP_State $\n ����� ����: 255.255.255.0$\n � ���������� ���������, ����� ��`
	FunctionEnd

;--------------------------------------------------------------------------
;������� ����������� ����������	

	Function "SwitchFirewallOff"
    ClearErrors
    ExecWait 'netsh advfirewall set allprofiles state off'
  
    SimpleFC::IsFirewallEnabled
    Pop $0 ; return error(1)/success(0)
    Pop $1 ; return 1=Enabled/0=Disabled
  
    ${If} $0 == 0 ; ���� ������ �� ���������
      ${If} $1 == 1 ;� ���� ������� �� ��� �������
        MessageBox MB_OK `������ ���������� �����������. �� ������ ������ ������� firewall!$\n ��� ������: $0 $\n ������ firewall: $1 $\n��������� ��� ������� ����� ������ ����������$\n � ���������� ���������, ����� ��`
      ${EndIf}
    ${Else}
        MessageBox MB_OK `��������� ����������� ������ ��� ������� ��������� ����������. �������� ��� �������`
    ${EndIf}
	FunctionEnd

;--------------------------------------------------------------------------
;������� ����������� ������ "����� ���������". ��������� ���������� �������� �������� ������������
	Function "SuchAMessage"
    WriteRegDword HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" "EnableLUA"  0
    IfErrors 0 +2
      MessageBox MB_OK `������ ��������� ����� �����������. ��������� ���������� �������� �������� �������$\n � ���������� ���������, ����� ��`
	FunctionEnd 
	
;--------------------------------------------------------------------------
;������� ����������� ������������ mkpa/mkpa

	Function "AddNewUser"
    ClearErrors
    ExecWait "net user /expires:never /add $Usr_State $Pswd_State"
    
    ExecWait "net localgroup �������������� mkpa /add"
    IfErrors 0 +2
      ExecWait "net localgroup Administrators mkpa /add"
	FunctionEnd
;--------------------------------------------------------------------------
;������� ������������� �������� ������������, ����������� �������� �����������
	Function "SecurityPolicies"
    ClearErrors
    FILE "${PROJECT_ROOT}\Vendor\Lsa_new.reg"
    ExecWait "Regedit /s Lsa_new.reg"
    IfErrors 0 +3
      MessageBox MB_OK `������ ��������� ������� ������������.`
      Delete "$INSTDIR\������\Lsa_new.reg"
	FunctionEnd
	
  Function "DCOM_Settings"
    SetOutPath "$TEMP"
    FILE /r "${PROJECT_ROOT}\Vendor\dcomperm.exe"
    ;	FILE /r "Vendor\ntrights.exe"
    
    ;	ExecWait 'sc create "QCOMServer1" binPath= "$INSTDIR\������ 1\comapp1.exe"'
    ;	ExecWait "sc config QCOMServer1 obj= .\mkpa password= mkpa"
    
    ;ma - ��������� ������� � ������
    ExecWait `DComPerm.exe -ma set "���" permit level:l,r`
    ExecWait `DComPerm.exe -ma set "mkpa" permit level:l,r`
    ExecWait `DComPerm.exe -ma set "REMOTE INTERACTIVE LOGON" permit level:l,r`
    ExecWait `DComPerm.exe -ma set "��������� ����" permit level:l,r`
    
    #ml - ��������� ������� �� ������
    ExecWait `DComPerm.exe -ml set "���" permit level:l,r,la,rl,ra`
    ExecWait `DComPerm.exe -ml set "mkpa" permit level:l,r,la,rl,ra`
    ExecWait `DComPerm.exe -ml set "REMOTE INTERACTIVE LOGON" permit level:l,r,la,rl,ra`
    ExecWait `DComPerm.exe -ml set "��������� ����" permit level:l,r,la,rl,ra`
    
    #da - ��������� ������� � ��� ��������� �� ���������
    ExecWait `DComPerm.exe -da set "���" permit level:l,r`
    ExecWait `DComPerm.exe -da set "mkpa" permit level:l,r`
    ExecWait `DComPerm.exe -da set "REMOTE INTERACTIVE LOGON" permit level:l,r`
    ExecWait `DComPerm.exe -da set "��������� ����" permit level:l,r`
    
    #dl - ��������� ������� �� ��� ���������� �� ���������
    ExecWait `DComPerm.exe -dl set "���" permit level:l,r,la,rl,ra`
    ExecWait `DComPerm.exe -dl set "mkpa" permit level:l,r,la,rl,ra`
    ExecWait `DComPerm.exe -dl set "REMOTE INTERACTIVE LOGON" permit level:l,r,la,rl,ra`
    ExecWait `DComPerm.exe -dl set "��������� ����" permit level:l,r,la,rl,ra`
  
  ;	ExecWait "$TEMP\ntrights.exe +r SeServiceLogonRight -u mkpa"
  
  ;	FILE /r "Vendor\instsrv.exe"
  ;	FILE /r "Vendor\srvany.exe"
  
  ;    ExecWait "$TEMP\instsrv.exe ResManStart $TEMP\srvany.exe"
  ;    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Services\ResManStart\Parameters" "Application" '"$PROGRAMFILES\Informtest\VISA\resman -o"'
  ;    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Services\ResManStart" "ImagePath" '"$PROGRAMFILES\Informtest\VISA\resman -o"'
  
  ;    ExecWait "sc config ResManStart depend= QCOMServer1"
  ;    ExecWait "sc config ResManStart start= auto"
  ;    ExecWait "sc config QCOMServer1 start= auto"
  
  FunctionEnd
	
;--------------------------------------------------------------------------
;�������, ���������� ����� ����� ������� ���������

  Function .onInit
           ;��������� ������ �� �������� ����������� �� �������, ���� ����

           StrCpy $IP_State "������� ���� IP �����"
           StrCpy $Usr_State "��� ������������ ������ ���� �����������"
           StrCpy $Pswd_State "��� ������ ������ ���� �����������"

           ReadRegStr $IP_Client_State HKLM "Software\Cometa\��� ���� ���" "IP_client_only"
           ${If} $IP_Client_State == ""
                 StrCpy $IP_Client_State "192.168.1.10"
           ${EndIf}

           ReadRegStr $IP_Srv1_State HKLM "Software\Cometa\��� ���� ���" "IP_srv1_only"
           ${If} $IP_Srv1_State == ""
                 StrCpy $IP_Srv1_State "192.168.1.101"
           ${EndIf}

           ReadRegStr $IP_Srv2_State HKLM "Software\Cometa\��� ���� ���" "IP_srv2_only"
           ${If} $IP_Srv2_State == ""
                 StrCpy $IP_Srv2_State "192.168.1.201"
           ${EndIf}
           
           ReadRegStr $IP_Srv3_State HKLM "Software\Cometa\��� ���� ���" "IP_srv2_only"
           ${If} $IP_Srv3_State == ""
                 StrCpy $IP_Srv3_State "192.168.1.13"
           ${EndIf}
           
           ReadRegStr $IP_Mon_State HKLM "Software\Cometa\��� ���� ���" "IP_mon_only"
           ${If} $IP_Mon_State == ""
                 StrCpy $IP_Mon_State "192.168.1.11"
           ${EndIf}

           ReadRegStr $IP_ASN_State HKLM "Software\Cometa\��� ���� ���" "IP_ASN_only"
           ${If} $IP_ASN_State == ""
                 StrCpy $IP_ASN_State "192.168.1.14"
           ${EndIf}

           ReadRegStr $IP_MBK07_State HKLM "Software\Cometa\��� ���� ���" "IP_MBK07_only"
           ${If} $IP_MBK07_State == ""
                 StrCpy $IP_MBK07_State "192.168.1.13"
           ${EndIf}

           ReadRegStr $IP_M4_State HKLM "Software\Cometa\��� ���� ���" "IP_M4_only"
           ${If} $IP_M4_State == ""
                 StrCpy $IP_M4_State "192.168.1.25"
           ${EndIf}
           
           ReadRegStr $Usr_State HKLM "Software\Cometa\��� ���� ���" "Srv1_usr"
           ${If} $Usr_State == ""
                 StrCpy $Usr_State "mkpa"
           ${EndIf}

           ReadRegStr $Pswd_State HKLM "Software\Cometa\��� ���� ���" "Srv1_pswd"
           ${If} $Pswd_State == ""
                 StrCpy $Pswd_State "mkpa"
           ${EndIf}

           
  FunctionEnd
;==========================================================================
;******************* ���������� ����� ����� �������!!! ********************
;==========================================================================
!include Permissioner.nsh
;==========================================================================
;**************************************************************************
;==========================================================================