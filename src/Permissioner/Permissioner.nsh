  !IFNDEF PERMISSIONER_NSH
  !define PERMISSIONER_NSH

;==========================================================================
; ����������� ��������������� �������

  !include "MUI2.nsh"                                ;����� �������� ���������
  !include LogicLib.nsh                              ;����������� �������������
  !include nsDialogs.nsh                             ;�������� ����������� ����
  !include ${PROJECT_ROOT}\Plugins\include\ValidateIPAdress.nsh      ;�������� ������������ ����� IP-������

;==========================================================================
;���� �������

;--------------------------------------------------------------------------
;�������-������, �������������� ��������� ������������!

  !macro EnableRadio IP_STATE PART_NAME 
    ${NSD_SetText} $IP ${IP_STATE}
    ${NSD_SetText} $Usr $Usr_State
    ${NSD_SetText} $Pswd $Pswd_State
    

    StrCpy $IP_RegName "IP_${PART_NAME}"
    StrCpy $Usr_RegName "${PART_NAME}_usr"
    StrCpy $Pswd_RegName "${PART_NAME}_pswd"
    ;MessageBox MB_OK "IP - $IP, Usr - $Usr, Pswd - $Pswd, $IP_RegName $Usr_RegName $Pswd_RegName" //���������� ���������
    call IPAddrTextChange ;����� ������� �� ����������� ����� ���� ��������� �������� ������������ ��� IP ������
  !macroend
  
;--------------------------------------------------------------------------
;���� �������, ������������ ������, ��������� ����

  Function EnClRadio
    !insertmacro EnableRadio $IP_Client_State "Client"
  FunctionEnd

  Function EnS1Radio
    !insertmacro EnableRadio $IP_Srv1_State "Srv1"
  FunctionEnd

  Function EnS2Radio
    !insertmacro EnableRadio $IP_Srv2_State "Srv2"  
  FunctionEnd
  
  Function EnS3Radio
   !insertmacro EnableRadio $IP_Srv3_State "Srv3"  
  FunctionEnd

  Function EnMonRadio
   !insertmacro EnableRadio $IP_Mon_State "Mon"  
  FunctionEnd
  
  Function EnASNRadio
   !insertmacro EnableRadio $IP_ASN_State "ASN"  
  FunctionEnd
  
  Function En07Radio
   !insertmacro EnableRadio $IP_MBK07_State "MBK07"  
  FunctionEnd
  
  Function EnM4Radio
   !insertmacro EnableRadio $IP_M4_State "M4"  
  FunctionEnd
;--------------------------------------------------------------------------

;--------------------------------------------------------------------------
;�������, ����������� �������� ����� �� ������, ���������� IP-������ �������
;����� ���������� ������ ��� ����� ����� � ���� ���������

  Function IPAddrTextChange
    ClearErrors
    Pop $1 # $1 == $IP_mon
  
    ${NSD_GetText} $IP $0
    Push $0
    Call ValidateIP
    GetDlgItem $NextButton $HWNDPARENT 1
    
    ${If} ${Errors}
      EnableWindow $NextButton 0
    ${Else}
      EnableWindow $NextButton 1
    ${EndIf}

  FunctionEnd
  
;--------------------------------------------------------------------------
;�������, ��������� ��������

  Function PageCreate
    ReadRegStr $new HKLM "Software\Cometa\��� ���� ���" "Crab"
    ${If} $new == "(V)(^__^)(V)"
      abort
    ${EndIf}

    nsDialogs::Create 1018        ;������� ��������, � "���������" ������� � ����, 1018 - ��� ��� ������ ��������
    Pop $Dialog                   ;����������� ��� � ����������

    ${If} $Dialog == error
          MessageBox MB_OK "�������� �������� ��� �������� �������� � �����������. ���������� � ������������. $\n $Dialog"
          Abort
    ${EndIf}
    ${NSD_CreateLabel} 0% 0% 100% 18% "�� ���� �������� ������������ ����� ������������.  $\n\
                                      ����������, �������� ����, ������� ����� ��������� ������ ������.$\n\
                                      ��� �� ������� �������� IP-����� ������ ������."
    Pop $Label

/*******************************************     GROUP BOXES    **************************************************/
    ${NSD_CreateGroupBox} 0% 20% 49% 79% "���� ���"
    Pop $GBox
;==============
    ${NSD_CreateRadioButton} 2% 25% 35% 13% "������"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnClRadio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 2% 35% 15% 13% "������ 1"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnS1Radio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 18% 35% 15% 13% "������ 2"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnS2Radio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 34% 35% 15% 13% "������ 3"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnS3Radio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 2% 45% 35% 13% "�������"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnMonRadio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 2% 55% 20% 13% "���2 ���-�"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnASNRadio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 2% 65% 35% 13% "�� ���-07"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton En07Radio ;TODO ������� ����� ����� �������� EnDisableRadio
    
    ${NSD_CreateRadioButton} 25% 55% 20% 13% "���-��"
    Pop $RadioButton
    ${NSD_OnClick} $RadioButton EnM4Radio ;TODO ������� ����� ����� �������� EnDisableRadio
;==============
/*******************************************     GROUP BOXES    **************************************************/
    ${NSD_CreateGroupBox} 51% 20% 49% 79% "������� ������������"
    Pop $GBox
;==============    
;IP 
  	${NSD_CreateLabel} 54% 28% 13% 8% "IP-�����:"
  	Pop $Label
  
  	${NSD_CreateText} 54% 36% 40% 9% $IP_State
    Pop $IP
;�����
  	${NSD_CreateLabel} 54% 48% 13% 8% "�����:"
  	Pop $Label

    ${NSD_CreateText} 54% 56% 40% 9% $Usr_State
    Pop $Usr
;������
    ${NSD_CreateLabel} 54% 68% 13% 8% "������:"
    Pop $Label

    ${NSD_CreateText} 54% 76% 40% 9% $Pswd_State
    Pop $Pswd
;=====================================================================================================================
;IP ����� ��������� ��������������, �����/������ �� ��������� - ���������. �� ��� ������������, ����� �� ���� ���������!
    EnableWindow $IP 1
    EnableWindow $Usr 0
    EnableWindow $Pswd 0

    ${NSD_OnChange} $IP IPAddrTextChange

#   ����������� ����� �������� IP �������, �.�. ��������� ������ 2 ���� �����, �������� ����������� �����!
#   call IPAddrTextChange
    
    nsDialogs::Show

  FunctionEnd #Function PageCreate
  
;--------------------------------------------------------------------------
;�������, �������������� ��� ����� �� ��������

  Function PageLeave
    ${NSD_GetText} $IP $IP_State

    ${NSD_GetText} $Usr $Usr_State
    ${NSD_GetText} $Pswd $Pswd_State
 
    WriteRegStr HKLM "Software\Cometa\��� ���� ���" $IP_RegName $IP_State
    WriteRegStr HKLM "Software\Cometa\��� ���� ���" $Usr_RegName  $Usr_State
    WriteRegStr HKLM "Software\Cometa\��� ���� ���" $Pswd_RegName $Pswd_State
    
    #�����������, �.�. ������������ ������ 1 ��� ��������� �����
    #StrCpy $Err "0"
    DetailPrint "�����: $IP_State"
  FunctionEnd
;==========================================================================
;MessageBox MB_OK "V(^__^)V" - ����-��������
;MessageBox MB_OK "(V)(^,,,^)(V)" - �����-������-��������
;MessageBox MB_OK "\(0o0)/ - ���� ��������"

;MessageBox MB_OK  `(\.../) (\.../) (\.../) (\.../) $\n ( *.*) ('.'= ) ('.'= ) ('.'= ) $\n (")_(") (")_(") (")_(") (")_(") $\n ��������� ��������-����������`
!endif