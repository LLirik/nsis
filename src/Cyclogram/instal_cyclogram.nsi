  !include "MUI2.nsh"


  !include "${PROJECT_ROOT}\Plugins\include\save_old.nsh"
  !include "${PROJECT_ROOT}\Plugins\include\MUI_EXTRAPAGES.nsh"

  !addplugindir "${PROJECT_ROOT}\Plugins\bin"
;--------------------------------------------------------------------------
;General

  Name "���������� ����������"
  OutFile "Install_cyclogram.exe"

  RequestExecutionLevel admin

;--------------------------------------------------------------------------

;==========================================================================
;��������� ����������

  !define MUI_ABORTWARNING      ;�������� �������������� ��������� ��� ������ ���������
  !define MUI_ABORTWARNING_TEXT "�� ������ �������� ��������� �������?"

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "orange.bmp"
  !define MUI_WELCOMEFINISH_BITMAP "orange.bmp"
  !define MUI_ICON "cycle.ico"

  !define MUI_FINISHPAGE_NOAUTOCLOSE ;�� ��������� �������� � �������� ���������

;===========================================================================
;��������

  !insertmacro MUI_PAGE_COMPONENTS

  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

;===========================================================================
;����
  ;Add 1st language
  !insertmacro MUI_LANGUAGE "English"

  ;Set up install lang strings for 1st lang
  ${ReadmeLanguage} "${LANG_ENGLISH}" \
          "Read Me" \
          "Please review the following important information." \
          "About $(^name):" \
          "$\n  Click on scrollbar arrows or press Page Down to review the entire text."

  ;Set up uninstall lang strings for 1st lang
  ${Un.ReadmeLanguage} "${LANG_ENGLISH}" \
          "Read Me" \
          "Please review the following important Uninstall information." \
          "About $(^name) Uninstall:" \
          "$\n  Click on scrollbar arrows or press Page Down to review the entire text."


  ;Add 2nd language
  !insertmacro MUI_LANGUAGE "Russian"

  ;set up install lang strings for second lang
  ${ReadmeLanguage} "${LANG_RUSSIAN}" \
          "������ ���������" \
          "���������� ���������� ��������� ���� ����������." \
          "������ � ������������ � ��� ���������:" \
          "$\n  ��� ��������� ����������� ������� ��������� ��� ������� PageDown."

;===========================================================================
;Installer Sections
;---------------------------------------------------------------------------

Section "���������� ����������"
        SetRebootFlag true
        SetOutPath "$PROGRAMFILES\Cometa\���������� ����������"
        File "�� ����\cyclogram\*.*"
        WriteRegDword HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" "EnableLUA"  0
        IfErrors 0 +2
            MessageBox MB_OK `������ ��������� ����� �����������. ��������� ���������� �������� �������� �������$\n � ���������� ���������, ����� ��`
SectionEnd

Section ".NET 1 � 4"
        SetOutPath "$TEMP"
        FILE /r "dotNET"
        ExecWait '$TEMP\dotNET\dotNET_1.exe /Q'
        ExecWait "$TEMP\dotNET\dotNET_4.exe /Q /norestart"
SectionEnd

SectionGroup "MS Excel"
   Section "MS Excel"
        SetOutPath "$TEMP"
        FILE /r "Microsoft Office 2010.exe"
        ExecWait '"$TEMP\Microsoft Office 2010" -S'
        ExecWait '"$TEMP\Microsoft Office 2010\setup.exe" /config Enterprise.WW\config.xml'
        WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\RunOnce" "Setup_ms_office_pia" "$TEMP\Microsoft Office 2007\o2007pia.msi"
   SectionEnd
   Section "Word Dev Tools"
        ExecWait "$TEMP\Microsoft Office 2007\o2007pia.msi"
   SectionEnd
SectionGroupEnd

