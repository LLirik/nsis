Mesa library
------------

Measurement Studio is dynamically linked with the Mesa OpenGL-compatible*
3-D graphics library (MESA.DLL), which is installed by the Measurement Studio
installer, and by LabWindows/CVI Distribution Kit installers.

This version of Mesa is a slightly modified version of Mesa 3.0,
which is released under the GNU Library Public License.

The unmodified version of Mesa allocates a large amount of memory
for stencil buffers and accumulation buffers regardless of whether
the application needs these features, and has a fairly large
maximum render context size.  The Measurement Studio version of Mesa
has been compiled with a smaller render context and certain
features disabled in order to use less memory.  In addition,
Phong shading support has been added to the library to
improve the appearance of some Measurement Studio controls.

In compliance with the GNU Library Public License (LGPL), the source
code to Mesa including these modifications, and the text of the
license, are available on the Measurement Studio 6.0 CD-ROM in the
/redist/mesa-src directory.  These files may be freely redistributed
and modified under the terms of this license.

* OpenGL(R) is a registered trademark of Silicon Graphics, Inc.
